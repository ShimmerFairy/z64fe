/** \file
 *
 *  \brief Defines all the spicks and specks for the RCP's current state.
 *
 */

#pragma once

#include "RCP/CC.hpp"

#include <cstdint>
#include <map>
#include <iosfwd>

namespace RCP {
    class GeometryMode {
      public:
        enum class Flags {
            ZBUFFER,
            SHADE,
            CULL_FRONT,
            CULL_BACK,
            FOG,
            LIGHTING,
            TEXTURE_GEN,
            TEXTURE_GEN_LINEAR,
            SHADING_SMOOTH,
        };

      private:
        std::map<Flags, bool> flagmap;

        uint32_t asMask(Flags which);

      public:
        GeometryMode();

        void clear_many(uint32_t bits);
        void set_many(uint32_t bits);

        void set(Flags thatone);
        void clear(Flags thatone);

        bool check(Flags thatone);

        friend std::ostream & operator<<(std::ostream & os, const GeometryMode & gm);
    };

    class BLMode {
      public:
        enum class Fields {
            AA_EN,
            Z_CMP,
            Z_UPD,
            IM_RD,
            CLR_ON_CVG,
            CVG_DST,
            ZMODE,
            CVG_X_ALPHA,
            ALPHA_CVG_SEL,
            FORCE_BL,
            P1, P2, M1, M2, A1, A2, B1, B2,
        };

        enum class CvgDst {
            CLAMP,
            WRAP,
            FULL,
            SAVE,
        };

        enum class ZMode {
            OPA,
            INTER,
            XLU,
            DEC,
        };

        enum class ParamP {
            IN,
            MEM,
            BL,
            FOG,
        };

        typedef ParamP ParamM;

        enum class ParamA {
            IN,
            FOG,
            SHADE,
            ZERO,
        };

        enum class ParamB {
            ONEMINUSA,
            AMEM,
            ONE,
            ZERO,
        };

      private:
        ParamP getP(uint8_t val);
        ParamM getM(uint8_t val);
        ParamA getA(uint8_t val);
        ParamB getB(uint8_t val);

      public:
        bool aa_en       = false;
        bool z_cmp       = false;
        bool z_upd       = false;
        bool im_rd       = false;
        bool clr_on_cvg  = false;
        CvgDst cvg       = CvgDst::CLAMP;
        ZMode zm         = ZMode::OPA;
        bool cvgXalpha   = false;
        bool alphacvgsel = false;
        bool forceBL     = false;

        ParamP c1p = ParamP::IN;
        ParamM c1m = ParamM::IN;
        ParamA c1a = ParamA::IN;
        ParamB c1b = ParamB::ONEMINUSA;

        ParamP c2p = ParamP::IN;
        ParamM c2m = ParamM::IN;
        ParamA c2a = ParamA::IN;
        ParamB c2b = ParamB::ONEMINUSA;

        void setMode(uint32_t val);
    };

    std::ostream & operator<<(std::ostream & os, const BLMode & blm);

    // note: currently assuming hardware 2.0
    class OtherMode {
      public:
        enum class Fields {
            ALPHACOMPARE,
            ZSRCREL,
            RENDERMODE,
            ALPHADITHER,
            RGBDITHER,
            COMBKEY,
            TEXCONV,
            TEXFILT,
            TEXLUT,
            TEXLOD,
            TEXDETAIL,
            TEXPERSP,
            CYCLETYPE,
            PIPELINE,
        };

      private:
        Fields fromShiftH(uint8_t sft);
        Fields fromShiftL(uint8_t sft);

      public:
        enum class AlphaCompare {
            NONE,
            THRESHOLD,
            DITHER,
        };

        enum class AlphaDither {
            PATTERN,
            NOTPATTERN,
            NOISE,
            DISABLE,
        };

        enum class RGBDither {
            MAGICSQ,
            BAYER,
            NOISE,
            DISABLE,
        };

        enum class TexConv {
            CONV,
            FILTCONV,
            FILT,
        };

        enum class TexFilt {
            POINT,
            BILERP,
            AVERAGE,
        };

        enum class TexLUT {
            NONE,
            RGBA16,
            IA16,
        };

        enum class TexDetail {
            CLAMP,
            SHARPEN,
            DETAIL,
        };

        enum class CycleType {
            ONE,
            TWO,
            COPY,
            FILL,
        };

        AlphaCompare ac = AlphaCompare::NONE;
        bool zs         = false;
        BLMode bl;
        AlphaDither ad  = AlphaDither::PATTERN;
        RGBDither cd    = RGBDither::MAGICSQ;
        bool ck         = false;
        TexConv tc      = TexConv::CONV;
        TexFilt tf      = TexFilt::POINT;
        TexLUT tt       = TexLUT::NONE;
        bool tl         = false;
        TexDetail td    = TexDetail::CLAMP;
        bool tp         = false;
        CycleType cyc   = CycleType::ONE;
        bool pm         = false;

        void setValueH(uint8_t sft, uint8_t siz, uint32_t rawval);
        void setValueL(uint8_t sft, uint8_t siz, uint32_t rawval);

        void setAllH(uint32_t rawval);
        void setAllL(uint32_t rawval);
    };

    std::ostream & operator<<(std::ostream & os, const OtherMode & om);

    struct CCCycle {
        CC::ColorA ca = CC::ColorA::COMBINED;
        CC::ColorB cb = CC::ColorB::COMBINED;
        CC::ColorC cc = CC::ColorC::COMBINED;
        CC::ColorD cd = CC::ColorD::COMBINED;
        CC::AlphaA aa = CC::AlphaA::COMBINED;
        CC::AlphaB ab = CC::AlphaB::COMBINED;
        CC::AlphaC ac = CC::AlphaC::LOD_FRACTION;
        CC::AlphaD ad = CC::AlphaD::COMBINED;
    };

    std::ostream & operator<<(std::ostream & os, const CCCycle & ccc);
}
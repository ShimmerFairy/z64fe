/** \file
 *
 *  \brief Defines a Camera object for OpenGL viewports
 *
 *  This is meant to aid in manipulating the view in a more structured and
 *  sensible way than something ad-hoc.
 *
 */

#pragma once

#include "RCP/Vector.hpp"
#include "RCP/Matrix.hpp"

#include <cmath>
#include <iosfwd>

namespace RCP {
    class Camera {
      private:
        // for making linear movements relative instead of absolute (i.e. we
        // want dimensionless movements, like we get for free with radians).
        double linfac = 1.0;
        double lastw = 320.0;
        double lasth = 240.0;

        void recalc_linfac();

      public:
        // viewing items
        Vector pos;
        Vector focus;
        Vector up;

        // projection items
        double yfov;
        double aspect;
        double nearZ;
        double farZ;

        Camera();

        void orbitWorldY(double rads);
        void orbitViewX(double rads);

        void pan(double xdelta, double ydelta);

        void rotateViewZ(double rads);

        void dolly(double dist); // positive increases distance (moves away)

        Matrix viewMatrix() const;

        void zoomChange(double delta);

        void setAspect(double w, double h);
        void setDepthRange(double n, double f);

        Matrix projMatrix() const;
    };

    std::ostream & operator<<(std::ostream & os, const Camera & cm);
}
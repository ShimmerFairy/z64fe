/** \file
 *
 *  \brief Defines the 4x4 Matrix class used for DL matrices; and also
 *         internally
 *
 *  We use double precision internally, but handle the native format of RCP
 *  matrices as well.
 *
 *  (used internally as well since the other options, Qt and glm, force
 *  column-major on us, even though row-major is vastly preferable, and we need
 *  to implement some measure of matrices ourselves already).
 *
 */

#pragma once

#include "Vector.hpp"

#include <array>
#include <initializer_list>
#include <vector>
#include <ostream>

namespace RCP {
    class Matrix {
      private:
        std::array<double, 16> parts;

        class MRow {
          private:
            Matrix & ref;
            size_t row;

          public:
            MRow(Matrix & mref, size_t rnum);

            double & operator[](size_t col);
        };

        class ConstMRow {
          private:
            const Matrix & ref;
            size_t row;

          public:
            ConstMRow(const Matrix & mref, size_t rnum);

            double operator[](size_t col) const;
        };

        void ip_recurse(std::vector<std::vector<size_t>> & ref, std::vector<size_t> prefix);
        std::vector<std::vector<size_t>> idx_perm();

        double sgn(std::vector<size_t> vec);

      public:
        Matrix();
        Matrix(double scale);
        Matrix(std::initializer_list<double> nparts);

        std::array<double, 16> rawRowMajor() const;
        std::array<double, 16> rawColumnMajor() const;

        decltype(parts)::iterator begin();
        decltype(parts)::iterator end();

        double & at(size_t row, size_t col);
        double at(size_t row, size_t col) const;

        MRow operator[](size_t row);
        ConstMRow operator[](size_t row) const;

        static Matrix identity();
        static Matrix orthographic(Point ll, Point ur, double nearZ, double farZ);
        static Matrix perspective(double yfov, double aspect, double nearZ, double farZ);
        static Matrix frustum(Point ll, Point ur, double nearZ, double farZ);
        static Matrix lookAt(Vector eye, Vector seeing, Vector up);

        Matrix & operator+=(const Matrix & that);
        Matrix & operator-=(const Matrix & that);
        Matrix & operator*=(const Matrix & that);
        Matrix & operator*=(const double & scale);
        Matrix & operator/=(const double & scale);

        double det();

        bool singular();

        // mutating

        Matrix & transpose();
        Matrix & invert();
        Matrix & rotate(double rad, Vector around);
        Matrix & translate(Vector dir);
        Matrix & scale(Vector factor);

        // non-mutating

        Matrix transposition() const;
        Matrix inverse() const;
        Matrix rotation(double rad, Vector around) const;
        Matrix translation(Vector dir) const;
        Matrix scaled(Vector factor) const;
    };

    Matrix operator+(const Matrix & a, const Matrix & b);
    Matrix operator-(const Matrix & a, const Matrix & b);

    Matrix operator*(const Matrix & a, const Matrix & b);
    Matrix operator*(const Matrix & a, const double & b);
    Matrix operator*(const double & a, const Matrix & b);

    Vector operator*(const Matrix & a, const Vector & b);
    Vector operator*(const Vector & a, const Matrix & b);

    Vector & operator*=(Vector & a, const Matrix & b);

    Matrix operator/(const Matrix & a, const double & b);

    bool operator==(const Matrix & a, const Matrix & b);
    bool operator!=(const Matrix & a, const Matrix & b);

    std::ostream & operator<<(std::ostream & os, const Matrix & mtx);
}
/** \file
 *
 *  \brief Define vectors for use in the RCP
 *
 */

#pragma once

#include <array>
#include <ostream>

namespace RCP {
    /** \brief Simple struct representing a point.
     *
     *  This is just a struct for when a point makes better sense conceptually
     *  than a vector, even though data-wise they're the same thing.
     *
     */
    struct Point {
        double x = 0.0;
        double y = 0.0;
        double z = 0.0;
    };

    /** \brief Class representing a 3D Vector
     *
     *  This class is used for doing general 3D math with vectors. Even though
     *  we use projective geometry (like anyone using OpenGL), the fourth
     *  ordinate is never specified by N64 model data, so there's nothing worth
     *  handling on that dimension with respect to doing math in this program.
     *
     *  <b>This is not for RCP vertices!</b> For that, you want the Vertex class
     *  instead, which may hold a Vector internally.
     *
     *  \note The multiplication operator is not defined between two Vector
     *        objects, since the meaning is inherently ambiguous. If you wish to
     *        use operators to multiply Vector objects, petition your local C++
     *        committee to allow for custom operators, so we can define things
     *        like \c operator× and \c operator⋅ for this.
     *
     */
    class Vector {
      private:
        std::array<double, 3> pointTo; ///< The point we're pointing to from the origin

      public:
        /** \brief Default constructor
         *
         *  Creates a Vector pointing to the origin.
         *
         */
        Vector();

        /** \brief Value-based constructor
         *
         *  Creates a Vector with the coordinates given as arguments.
         *
         *  \param[in] X The x-axis ordinate
         *  \param[in] Y The y-axis ordinate
         *  \param[in] Z The z-axis ordinate
         *
         */
        Vector(double X, double Y, double Z);


        /** \brief Adds one Vector to another
         *
         *  This adds the Vector given on the right to the Vector object on the
         *  left in component-wise fashion.
         *
         *  \param[in] that Vector to add to the one on the left
         *
         *  \returns A reference to the modified Vector.
         *
         */
        Vector & operator+=(const Vector & that);

        /** \brief Subtracts one Vector from another
         *
         *  This does component-wise subtraction of the Vector given on the
         *  right from the Vector object on the left.
         *
         *  \param[in] that Vector to subtract from the one on the left.
         *
         *  \returns A reference to the modified Vector.
         *
         */
        Vector & operator-=(const Vector & that);

        /** \brief Scales the Vector by given factor
         *
         *  This scales each component in the Vector by the factor given on the
         *  right-hand side. Note that you can effectively divide by using a
         *  scale factor whose absolute value is less than one.
         *
         *  \param[in] scale Factor by which to scale.
         *
         *  \returns A reference to the modified Vector.
         *
         */
        Vector & operator*=(const double & scale);

        /** \brief Scales the Vector by given factor
         *
         *  This scales each component in the Vector by the factor given on the
         *  right-hand side. Note that you can effectively multiply by using a
         *  scale factor whose absolute value is less than one.
         *
         *  \param[in] scale Factor by which to scale.
         *
         *  \returns A reference to the modified Vector.
         *
         */
        Vector & operator/=(const double & scale);


        /** \brief Makes a negated version of the Vector
         *
         *  This operator returns a copy of the Vector it's used on with each
         *  component negated (i.e. scaled by \c -1.0).
         *
         *  \returns A copy of the Vector with components negated.
         *
         */
        Vector operator-() const;


        /** \brief Tests equality of two Vectors.
         *
         *  Compares the components of each Vector, and returns \c true iff
         *  they're equal.
         *
         *  \param[in] that The other vector to compare against
         *
         *  \return A boolean; \c true if the Vectors are equal, \c false
         *          otherwise.
         *
         *  \note This implementation currently doesn't do fuzzy matching for
         *        the internal floating-point values.
         *
         */
        bool operator==(const Vector & that) const;

        /** \brief Tests inequality of two Vectors.
         *
         *  The inverse of <tt>operator==</tt>, in that it returns \c false iff
         *  the two Vector objects are equal.
         *
         *  \param[in] that The other vector to compare against
         *
         *  \return A boolean; \c false if the Vectors are equal, \c false
         *          otherwise.
         *
         */
        bool operator!=(const Vector & that) const;

        /** \brief Returns the Euclidean length of the vector
         *
         *  This method returns the Euclidean length of the vector,
         *  i.e. \f$\sqrt{x^2 + y^2 + z^2}\f$.
         *
         *  \returns Vector length
         *
         */
        double length() const;

        double x() const; ///< Returns x ordinate of the vector
        double y() const; ///< Returns y ordinate of the vector
        double z() const; ///< Returns z ordinate of the vector

        double & x(); ///< Returns modifiable x ordinate of the vector
        double & y(); ///< Returns modifiable y ordinate of the vector
        double & z(); ///< Returns modifiable z ordinate of the vector

        std::array<double, 3>::iterator begin();
        std::array<double, 3>::iterator end();

        std::array<double, 3>::const_iterator begin() const;
        std::array<double, 3>::const_iterator end() const;

        /** \brief Returns the dot product between two Vectors.
         *
         *  This does a dot product between the Vector being called on, and the
         *  one given as an argument.
         *
         *  \param[in] that The other Vector in this operation.
         *
         *  \returns A \c double representing the result of the dot product.
         *
         */
        double dot(const Vector & that) const;

        /** \name Mutating methods
         *
         *  These Vector methods mutate the Vector the function is called on,
         *  and return a reference to the modified Vector.
         *
         */
        ///@{

        /** \brief Normalizes the Vector to a unit vector.
         *
         *  This method simply normalizes the Vector (by dividing each component
         *  by the length), thus turning the Vector into a unit vector.
         *
         *  \returns A reference to the modified Vector.
         *
         */
        Vector & normalize();

        /** \brief Performs cross product between two Vectors, modifying the
         *         caller.
         *
         *  This performs the cross product operation between the two Vector
         *  objects. The expression <tt>a.cross(b)</tt> is mathematically \f$a
         *  \times b\f$. The result is then stored in <tt>a</tt>.
         *
         *  \param[in] that The vector on the right-hand side of the cross
         *                  operation.
         *
         */
        Vector & cross(const Vector & that);
        ///@}

        /** \name Non-mutating methods
         *
         *  These methods perform the same operations as the mutating methods
         *  provided, but with the obvious exception of not mutating the
         *  caller. Instead the mutating operation is performed on a \em copy of
         *  <tt>*this</tt>, with that copy then being returned.
         *
         */
        ///@{

        /** \brief Gives the normalized version of the Vector
         *
         *  This method simply returns a new Vector which is the normalized
         *  version of the one this function is called on.
         *
         *  \returns A new Vector that is a unit vector, i.e. the normalized
         *           version of the caller Vector.
         *
         */
        Vector normalized() const;

        /** \brief Returns the result of a cross product between two Vectors.
         *
         *  This function performs the cross product operation between two
         *  Vector objects, with <tt>a.cross(b)</tt> meaning \f$a \times b\f$, and
         *  returns a Vector representing this result.
         *
         *  \param[in] that The vector on the right-hand side of the cross
         *                  operation.
         *
         *  \returns A new Vector resulting from the cross product.
         *
         */
        Vector crossed(const Vector & that) const;
        ///@}
    };

    /** \brief Addition between two Vector objects.
     *
     *  Does addition between the two Vector objects and returns a new Vector.
     *
     *  \param[in] a,b The vectors to add together
     *
     *  \returns A new vector, the result of adding the two Vectors.
     *
     */
    Vector operator+(const Vector & a, const Vector & b);

    /** \brief Subtraction between two Vector objects.
     *
     *  Does subtraction between the two Vector objects and returns a new
     *  Vector.
     *
     *  \param[in] a,b The vectors to subtract.
     *
     *  \returns A new vector, the result of subtracting the two Vectors.
     *
     */
    Vector operator-(const Vector & a, const Vector & b);

    /** \brief Scales a Vector by a scalar factor
     *
     *  Scales each component of the given Vector by the given scale factor, and
     *  returns the resulting Vector.
     *
     *  \param[in] a The Vector to scale
     *
     *  \param[in] b The scale factor
     *
     *  \returns A new Vector that's the scaled version of the input Vector.
     *
     */
    Vector operator*(const Vector & a, const double & b);

    /** \brief Scales a Vector by a scalar factor
     *
     *  Scales each component of the given Vector by the given scale factor, and
     *  returns the resulting Vector.
     *
     *  \param[in] a The scale factor
     *
     *  \param[in] b The Vector to scale
     *
     *  \returns A new Vector that's the scaled version of the input Vector.
     *
     */
    Vector operator*(const double & a, const Vector & b);

    /** \brief Scales a Vector by a scalar factor
     *
     *  Scales each component of the given Vector by the given scale factor, and
     *  returns the resulting Vector.
     *
     *  \param[in] a The Vector to scale
     *
     *  \param[in] b The scale factor
     *
     *  \returns A new Vector that's the scaled version of the input Vector.
     *
     */
    Vector operator/(const Vector & a, const double & b);

    /** \brief Writes human-readable form of Vector to an output stream.
     *
     *  Implements stream insertion for Vector objects, writing to the
     *  std::ostream a human-readable version of the given Vector.
     *
     *  \param[out] os The std::ostream to write to.
     *
     *  \param[in] vec The Vector to write to stream.
     *
     *  \returns A reference to the std::ostream written to, to enable chaining
     *           insertions.
     *
     */
    std::ostream & operator<<(std::ostream & os, const Vector & vec);
}

namespace std {
    /** \brief Provides hash function for RCP::Vector.
     *
     *  This specialization allows std::unordered_set to use RCP::Vector
     *  objects, or other objects that calculate RCP::Vector hashes.
     *
     */
    template<> struct hash<RCP::Vector> {
        size_t operator()(const RCP::Vector & rcpv) const {
            size_t res = std::hash<double>()(rcpv.x());
            res ^= std::hash<double>()(rcpv.y()) << 1;
            res ^= std::hash<double>()(rcpv.z()) << 2;

            return res;
        }
    };
}
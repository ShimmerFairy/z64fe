//-*-c++-*-
/** \file
 *
 *  \brief Implements the templated functions of stuff in Tile.hpp (just to keep
 *         the header file less cluttered-seeming).
 *
 */

#ifndef GUARD_RCP_TILE_TPP
#error "You can't include this file yourself!"
#endif

namespace RCP {
    template<typename RAIter>
    void TMEM::loadTile(const Tile & td, const RAIter & ramptr, uint16_t ramwidth) {
        inreset();

        size_t tidx = td.addressBytes();
        auto ridx = ramptr;

        ridx += ramwidth * td.upleftT;

        size_t num_lines = td.botrightT - td.upleftT + 1;
        size_t num_cols  = td.botrightS - td.upleftS + 1;

        // for moving to the first column we're grabbing
        size_t prepad = size_t(td.upleftS) * td.texelSizeBytes();

        // for moving to the next row
        size_t postpad = ramwidth - (prepad + num_cols * td.texelSizeBytes());

        // for padding TMEM if needed
        size_t leftover; // = 8 - ((num_cols * td.texelSizeBytes()) % 8);

        if (td.format == Image::Format::RGBA_32) {
            // despite being a 32-bit format, the calculation of leftover
            // bytes per line operates on units of 16 bits per texel, due to
            // how it's laid out in TMEM.
            leftover = 8 - ((num_cols * 2) % 8);

            for (size_t line = 0; line < num_lines; line++) {
                // move to first column in row
                ridx += prepad;

                // go over texels in row
                for (size_t col = 0; col < num_cols; col++) {
                    mlo(tidx++) = *ridx++; // R
                    mlo(tidx--) = *ridx++; // G
                    mhi(tidx++) = *ridx++; // B
                    mhi(tidx++) = *ridx++; // A
                }

                // for the next row:
                tidx += leftover; // pad word
                inflip();         // flip interleaving status
                ridx += postpad;  // get to next row in RAM
            }
        } else if (td.format == Image::Format::YUV_16) {
            // Since the YUV16 format, stored as UYVY, takes 32 bits per 2
            // texels, the leftover calculation is weird like RGBA32. This
            // plus the storage scheme means you need 8 texels (4 groups of
            // 32 bits in RAM) to make a full row.
            leftover = 8 - (num_cols % 8);

            for (size_t line = 0; line < num_lines; line++) {
                // move to first column
                ridx += prepad;

                for (size_t col = 0; col < num_cols; col++) {
                    mlo(tidx)   = *ridx++; // U
                    mhi(tidx++) = *ridx++; // first Y
                    mlo(tidx)   = *ridx++; // V
                    mhi(tidx++) = *ridx++; // second Y
                }

                // for the next row:
                tidx += leftover; // pad word
                inflip();         // flip interleaving status
                ridx += postpad;  // get to next row in RAM.
            }
        } else {
            // For all other formats, this is a pretty straightforward operation.
            leftover = 8 - ((num_cols * td.texelSizeBytes()) % 8);

            for (size_t line = 0; line < num_lines; line++) {
                ridx += prepad;

                for (size_t col = 0; col < num_cols; col++) {
                    for (size_t i = 0; i < td.texelSizeBytes() % 8) {
                        mat(tidx++) = *ridx++;
                    }
                }

                tidx += leftover;
                inflip();
                ridx += postpad;
            }
        }
    }

    template<typename RAIter>
    void TMEM::loadBlock(const Tile & td, const RAIter & ramptr, uint16_t last_txl_idx, ufix<1, 11> dxt) {
        inreset();

        ufix<1, 11> counter = 0;

        size_t tidx = td.addressBytes();
        auto ridx = ramptr;

        if (td.format == Image::Format::RGBA_32) {
            // note that the splitting nature of RGBA32 storage means your
            // textures have to have a width that's a multiple of 128 bits
            // (4 texels for RGBA32). Otherwise we'll pad. (XXX It's not
            // clear how LoadBlock handles 64-bit-and-not-128-bit images,
            // for now we'll assume "yes")

            while (std::distance(ramptr, ridx) <= (last_txl_idx * td.texelSizeBytes())) {
                for (size_t i = 0; i < 2; i++) {
                    mlo(tidx)     = *ridx++; // R
                    mlo(tidx + 1) = *ridx++; // G
                    mhi(tidx++)   = *ridx++; // B
                    mhi(tidx++)   = *ridx++; // A
                }
                counter += dxt;

                if (counter >= 1) {
                    counter -= 1;
                    if (tidx % 4) {
                        tidx += 2;
                    }
                    inflip();
                }
            }
        } else if (td.format == Image::Format::YUV_16) {
            while (std::distance(ramptr, ridx) <= (last_txl_idx * td.texelSizeBytes())) {
                for (size_t i = 0; i < 4; i++) {
                    mlo(tidx) = *ridx++;
                    mhi(tidx) = *ridx++;
                    tidx++;
                }
                counter += dxt;

                if (counter >= 1) {
                    counter -= 1;
                    if (tidx % 4) {
                        tidx += 2;
                    }
                    inflip();
                }
            }
        } else {
            while (std::distance(ramptr, ridx) <= (last_txl_idx * td.texelSizeBytes())) {
                for (size_t i =0; i < 8; i++) {
                    mat(tidx++) = *ridx++;
                }
                counter += dxt;

                if (counter >= 1) {
                    counter -= 1;
                    inflip();
                }
            }
        }
    }

    template<typename RAIter>
    void TMEM::loadTLUT(const Tile & td, const RAIter & ramptr, size_t amt) {
        // this command effectively quadruples the given palette, to ease
        // the use of simultaneous access. We emulate this because, similar
        // to the corner case of LoadBlock with a dxt of 0, someone could
        // "manually" load the palette with LoadTile or LoadBlock in its
        // quadrupled form.

        // note that palettes have to be 16-bit (RGBA or IA), so we can
        // lazily assume this is the case. If you're trying to load any
        // other type of image as a palette, it's your own fault we don't
        // handle it.

        size_t tidx = td.addressBytes() - 2048;
        auto ridx = ramptr;

        for (size_t i = 0; i < amt; i++) {
            uint8_t b1 = *ridx++;
            uint8_t b2 = *ridx++;

            for (size_t j = 0; j < 4; j++) {
                mhi(tidx++) = b1;
                mhi(tidx++) = b2;
            }
        }
    }
}
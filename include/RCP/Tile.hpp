/** \file
 *
 *  \brief Defines classes for tile descriptors and other needed things for
 *         handling textures.
 *
 */

#pragma once

#include "RCP/Image.hpp"
#include "Fixed.hpp"

#include <cstdint>

namespace RCP {
    struct Tile {
        Image::Format format;
        uint16_t u64_per_line;
        uint16_t TMEM_addr;
        uint8_t palno;
        bool mirrorS, mirrorT;
        uint8_t maskS, maskT;
        uint8_t shiftS, shiftT;
        ufix<10, 2> upleftS, upleftT;
        ufix<10, 2> botrightS, botrightT;
        bool clampS, clampT;

        size_t texelSizeBytes();
        size_t addressBytes();
    };

    /** \brief Class emulating the RCP's TMEM area.
     *
     *  This kinda low-level approach to texture memory is taken because there's
     *  a chance some textures are already "interlaced" when loaded into TMEM,
     *  which we have to account for in some way. We choose to emulate how TMEM
     *  actually works to make sure things work as expected for OoT/MM display
     *  lists.
     *
     *  We choose the logical layout used when textures are loaded into TMEM,
     *  which is a 64 bits x 256 rows lower half followed by a 64 bits x 256
     *  rows upper half. The physical layout involves 8 groups of 16x256 bytes,
     *  which we don't choose since it doesn't offer any apparent benefit over
     *  the logical layout. (The physical layout however does aid in
     *  understanding why specific image types are loaded in the way they are.)
     *
     *  This will surely not be the most efficient way for handling textures in
     *  \em your memory, but it sure beats having to divine the right dxt value
     *  to de-pre-interlace textures and hope we did it right.
     *
     */
    class TMEM {
      private:
        std::array<uint8_t, 4096> mem;

        bool interflag = false;
        void inflip();
        void inreset();

        uint8_t & mlo(size_t idx);
        uint8_t & mhi(size_t idx);
        uint8_t & mat(size_t idx);

      public:
        /** \brief Loads a rectangular portion of the current texture image into
         *         TMEM.
         *
         *  Interprets the current portion of RAM being pointed to as a series
         *  of rows of texels, and then takes a portion of that into TMEM.
         *
         *  \tparam RAIter A random-access iterator type. Should be inferrable
         *                 from the second parameter.
         *
         *  \param[in] td The tile descriptor used for loading.
         *
         *  \param[in] ramptr A pointer to the start of the texture in RAM. This
         *                    should reflect the address set in SETTIMG.
         *
         *  \param[in] ramwidth The number of texels in a row of the RAM's
         *                      texture. This is _not_ the width of the part
         *                      being loaded, that's contained in the tile
         *                      descriptor.
         *
         */
        template<typename RAIter>
        void loadTile(const Tile & td, const RAIter & ramptr, uint16_t ramwidth);

        /** \brief Loads a continuous stream of texels from RAM to TMEM.
         *
         *  This interprets the given area of RAM as a continuous stream of
         *  texels to load into TMEM. A counter is used to track the start/end
         *  of each row of the tile. The main functional differences from
         *  loadTile are:
         *
         *  1. You cannot load a portion of the current texture image (as if
         *     loading the entire texture image with loadTile)
         *
         *  2. You must pad the picture to units of 64-bits yourself, as opposed
         *     to letting loadTile do it for you.
         *
         *  \tparam RAIter A random-access iterator type.
         *
         *  \param[in] td The tile descriptor used for loading.
         *
         *  \param[in] ramptr A pointer to the start of the stream to load. This
         *                    should reflect the address set by SETTIMG.
         *
         *  \param[in] last_txl_idx The index (0-based of course) of the last
         *                          64-bit unit of the image, relative to
         *                          ramptr.
         *
         *  \param[in] dxt The amount by which to increment the "dxt" counter
         *                 per 64-bit unit, which helps determine rows of the
         *                 image. When this u1.11 number is greater than or
         *                 equal to 1.0, the loading process determines that the
         *                 next row has begun.
         *
         *  \warning Image widths and formats leading to a dxt value that's not
         *           a power of two are at risk for corruption after a number of
         *           rows, due to accumulated error. (The dxt counter <b>does
         *           not</b> reset to zero when a new row is determined.)
         *
         *  \todo Add a chart or formula somewhere for figuring out where this
         *        error starts happening?
         *
         */
        template<typename RAIter>
        void loadBlock(const Tile & td, const RAIter & ramptr, uint16_t last_txl_idx, ufix<1, 11> dxt);

        /** \brief Loads a palette into TMEM
         *
         *  This can be viewed as a convenience operation, in that you could
         *  accomplish the same task with the other loading commands. The
         *  difference is that this handles the duplication of palette entries
         *  for you.
         *
         *  Palettes are loaded into the high half of TMEM, each entry written
         *  four times (to allow four simultaneous reads from the palette, one
         *  per bank). Thus, each 64-bit word contains one palette color. This
         *  command handles the cloning for you, using other methods requires
         *  doing this manually and wasting RAM as a result.
         *
         *  \tparam RAIter A random-access iterator type.
         *
         *  \param[in] td The tile descriptor used for loading.
         *
         *  \param[in] ramptr A pointer to the start of the palette, reflecting
         *                    the address set by SETTIMG.
         *
         *  \param[in] amt The number of colors to load, usually 16 or 256.
         *
         */
        template<typename RAIter>
        void loadTLUT(const Tile & td, const RAIter & ramptr, size_t amt);
    };
}

#define GUARD_RCP_TILE_TPP
#include "RCP/Tile.tpp"
#undef GUARD_RCP_TILE_TPP
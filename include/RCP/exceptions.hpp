/** \file
 *
 *  \brief Define exception objects specific to the RCP, to keep the main
 *         exceptions file from being too insane.
 *
 */

#pragma once

#include "Exceptions.hpp"

namespace X {
    namespace RCP {
        namespace Matrix {
            class CantInvert : public Exception {
              public:
                std::string what() override;
            };
        }

        class BadSegment : public Exception {
          private:
            uint32_t badseg;

          public:
            BadSegment(uint32_t bs);

            std::string what() override;
        };

        class BadOtherMode : public Exception {
          private:
            std::string half;
            uint8_t given_sft;
            uint8_t given_siz;
            uint32_t given_val;

          public:
            BadOtherMode(std::string h, uint8_t gsft, uint8_t gsiz, uint32_t gval);

            std::string what() override;
        };

        namespace TMEM {
            class OutOfRange : public Exception {
              private:
                std::string where;
                size_t badidx;  ///< What OOR index was given.
                size_t bitsper; ///< Determines units of specific operation.

              public:
                OutOfRange(size_t bidx, size_t bper = 8, std::string wh = "");

                std::string what() override;
            };

            class OutOfRangeLow : public Exception {
              public:
                OutOfRangeLow(size_t bidx, size_t bper = 8);
            };

            class OutOfRangeHigh : public Exception {
              public:
                OutOfRangeHigh(size_t bidx, size_t bper = 8);
            };
        }
    }
}
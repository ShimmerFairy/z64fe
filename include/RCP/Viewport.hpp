/** \file
 *
 *  \brief Defines the class that handles OpenGL rendering for models.
 *
 */

#pragma once

#include "RCP/DisplayList.hpp"
#include "RCP/Matrix.hpp"
#include "RCP/Vertex.hpp"
#include "RCP/Camera.hpp"
#include "RCP/State.hpp"

#include "ROM.hpp"

#include <QOpenGLWidget>
#include <QOpenGLFunctions_4_5_Core>
#include <QMatrix4x4>

#include <array>
#include <vector>

namespace RCP {
    // IMPORTANT! The use of 4.5 is JUST TEMPORARY, it's just to pick
    // _something_. We'll bump it back down after heavy development is over, to
    // where we can drop it down. (And if we can't drop it down to a nice
    // minimum, we'll then spend time providing alternate implementations of ze
    // new functions, conditional compilation, and so on.)
    class Viewport : public QOpenGLWidget, protected QOpenGLFunctions_4_5_Core {
        Q_OBJECT

      private:
        DisplayList renderThis;

        ROM::ROM * therom;
        ROM::File objfile;

        Camera thecam;

        // RCP State stuff
        GeometryMode state_gm;
        OtherMode state_om;
        CCCycle state_cc_1;
        CCCycle state_cc_2;


        std::array<Vertex, 32> vtx_preGL;
        std::array<GLVertex, 32> vtx_postGL;

        bool cando = true; // tells us if it's OK to try to render.

        unsigned int movemod = 0; // easier for us than using QFlags
        QPointF mouse_anchor;

        ROM::File segmentFile(uint8_t seg);

        GLuint render_vao;
        GLuint render_shdprgm;

        void checkshd(GLuint sid);

        void drawNow(uint8_t * ebodata, size_t edsize);

      protected:
        void paintGL() override;
        void initializeGL() override;
        void resizeGL(int w, int h) override;

        void mousePressEvent(QMouseEvent * ev) override;
        void mouseMoveEvent(QMouseEvent * ev) override;
        void wheelEvent(QWheelEvent * ev) override;

        void keyPressEvent(QKeyEvent * ev) override;
        void keyReleaseEvent(QKeyEvent * ev) override;

      public slots:
        void changeDL(DisplayList nobj);

      public:
        Viewport(ROM::ROM * tr, ROM::File objf, QWidget * parent = nullptr);
    };
}
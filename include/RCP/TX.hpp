/** \file
 *
 *  \brief Defines the Texture Engine for storing and retrieving textures.
 *
 */

#pragma once

#include "RCP/Tile.hpp"

namespace RCP {
    /** \brief Manages the TX part of the RDP pipeline.
     *
     *  Note that this does \em not handle the aspect of TX that actually \em
     *  does stuff; that kind of graphical manipulation is best left to OpenGL,
     *  and we do just that. This rather handles all the "prep work" needed for
     *  handing off to OpenGL, since dumping a bunch of info the shader doesn't
     *  end up needing onto the GPU wouldn't be the smartest choice.
     *
     *  In any event, this class is meant to handle the settings of the TX part,
     *  and anything the RSP does that's very closely related to textures (since
     *  this is the first texture-manipulating part of the pipeline).
     *
     */
    class TX {
      public:
        enum class Detail {
            Clamp,
            Sharpen,
            Detail,
        };

        enum class TLUTMode {
            None,
            RGBA16,
            IA16,
        };

      private:
        TMEM txtmem;
        std::array<Tile, 8> tds;

        bool rsp_ison = false;
        ufix<0, 16> scaleS;
        ufix<0, 16> scaleT;

        size_t morelevels = 0;
        size_t curtile;

      public:
        bool perspCorrect;
        Detail detailLOD;
        bool useLOD;
        TLUTMode typeTLUT;

        void enableTextures();
        void disableTextures();
        bool textureState() const;

        void setScale(ufix<0, 16> ns, ufix<0, 16> nt);
        void setScaleS(ufix<0, 16> ns);
        void setScaleT(ufix<0, 16> nt);

        ufix<0, 16> getScaleS() const;
        ufix<0, 16> getScaleT() const;

        void setMoreLevels(size_t nml);
        size_t getMoreLevelCount() const;

        Tile & tile(size_t idx);
        void setCurrentTile(size_t idx);
        Tile & getCurrentTile();

        
    };
}

/** \file
 *
 *  \brief Definition of the classes used in RCP Vertices.
 *
 *  This implementation ends up using three different classes for vertices,
 *  which are outlined thus:
 *
 *  \li RCP::RawVertex is for collecting the data from the game in the first
 *      place. It's meant as a nice way to hold that info as it gets turned into
 *      data we can more easily use.
 *
 *  \li RCP::Vertex is for manipulating vertices as needed before we use it in
 *      OpenGL code. It's a structure meant to aid in these manipulations
 *      outside of OpenGL, not for uploading to your GPU.
 *
 *  \li RCP::GLVertex is for vertex data as it's about to be sent to your
 *      graphics card. It's more low-level than RCP::Vertex to allow for
 *      OpenGL's low-level attribute setup functions. Any manipulations on a
 *      GLVertex is expected to happen in shader code.
 *
 *  Having three vertex classes does seem a bit overblown, but it does feel like
 *  the best way to go about this. If we had to use just one, GLVertex would be
 *  best: RawVertex only makes loading the data initially easier, and pre-shader
 *  transformations can always be handled by on-the-spot conversions of Vector
 *  objects, but something like GLVertex is almost unavoidable for the GPU
 *  upload.
 *
 */

#pragma once

#include "RCP/Vector.hpp"

#include <cstdint>

namespace RCP {
    /** \brief The raw data version of a vertex.
     *
     *  This structure is meant to more easily handle the raw data taken from
     *  reading display lists. It mirrors the memory layout of the data
     *  itself. Note that we cheat a bit, using the dummy field of this
     *  structure as the 'tagged' part of the tagged union we use for color or
     *  normals.
     *
     */
    struct RawVertex {
        int16_t position[3];     ///< Position in space
        uint16_t dummy_normflag; ///< Dummy field; used as tag for union below
        int16_t tex_pos[2];      ///< Texture coordinate for vertex

        /** \brief Union for contextual bytes
         *
         *  These three bytes of the vertex have a meaning dependent on the
         *  current state of the RCP; if lighting is enabled, they're seen as a
         *  normal vector. Otherwise, they're red, green, and blue components,
         *  in that order. The use of a union for this is due to the fact that
         *  the interpretation changes the data type (signed vs. unsigned).
         *
         */
        union {
            uint8_t RGB[3];      ///< Red/Green/Blue color components
            int8_t normal[3];    ///< Normal vector of the vertex
        };

        uint8_t alpha;           ///< Alpha component
    };

    /** \brief The "more modern" version of a vertex.
     *
     *  This is the form that's manipulated as needed before rendering. The
     *  reason we have separate classes for rendering is that the Vector fields
     *  here are easier to manipulate before OpenGL, while GLVertex uses raw
     *  fixed-size arrays more appropriate for loading as a structure into
     *  OpenGL.
     *
     */
    struct Vertex {
        Vector position;
        Vector normal;

        double tex_pos[2];
        uint8_t RGBA[4];

        Vertex() = default;
        Vertex(const RawVertex & rv);
    };

    /** \brief Equality check for vertices
     *
     *  Mainly used by code for calculating the center/bounding box of objects,
     *  to make sure we don't needlessly include duplicate vertices in the
     *  calculation.
     *
     */
    bool operator==(const Vertex & a, const Vertex & b);

    /** \brief The OpenGL-usable version of a vertex.
     *
     *  This structure is meant to be part of the array uploaded to your GPU as
     *  part of the various attributes used by shader programs. It is thus not
     *  as easy to transform outside of shaders as Vertex.
     *
     */
    struct GLVertex {
        double position[3];
        double normal[3];
        double tex_pos[2];
        double RGBA[4];

        GLVertex() = default;
        GLVertex(const Vertex & v);
    };
        
}

namespace std {
    /** \brief Provides hash function for RCP::Vertex.
     *
     *  This specialization allows std::unordered_set to use RCP::Vertex
     *  objects.
     *
     */
    template<> struct hash<RCP::Vertex> {
        size_t operator()(const RCP::Vertex & rcpv) const {
            size_t res = std::hash<RCP::Vector>()(rcpv.position);
            res ^= std::hash<RCP::Vector>()(rcpv.normal) << 1;
            res ^= std::hash<double>()(rcpv.tex_pos[0]) << 2;
            res ^= std::hash<double>()(rcpv.tex_pos[1]) << 3;
            res ^= std::hash<uint8_t>()(rcpv.RGBA[0]) << 4;
            res ^= std::hash<uint8_t>()(rcpv.RGBA[1]) << 5;
            res ^= std::hash<uint8_t>()(rcpv.RGBA[2]) << 6;
            res ^= std::hash<uint8_t>()(rcpv.RGBA[3]) << 7;

            return res;
        }
    };
}
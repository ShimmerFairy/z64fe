/** \file
 *
 *  \brief Declare all the various exception types we use in operation.
 *
 */

#pragma once

#include <string>
#include <cstdint>
#include <vector>
#include <initializer_list>

/** \brief The base class for program exceptions.
 *
 *  This is the basic class for all exceptions used in z64fe. This does not
 *  inherit from std::exception by design; z64fe-based exceptions shouldn't
 *  identify themselves as standard exceptions.
 *
 *  This class defines a virtual what() method that must be implemented for a
 *  useful error message; this class provides a default that merely points this
 *  out. Usually the constructor of a subclass provides information to be used
 *  by the virtual.
 *
 */
class Exception {
  public:
    /** \brief Explanation of the error.
     *
     *  This virtual method explains what went wrong in a human-readable
     *  fashion. In other words, this is the user-facing error string to be
     *  shown. Although a default implementation is provided, you must override
     *  this method to provide useful error messages.
     *
     *  \returns A std::string containing the message.
     *
     */
    virtual std::string what();

    /** \brief Virtual destructor
     *
     *  This is a virtual destructor to ensure complete destruction of exception
     *  objects, regardless of what the actual pointer/reference/etc. type is.
     *
     */
    virtual ~Exception() = default;
};

/** \brief Namespace for all exception objects.
 *
 *  This namespace holds every subclass of the Exception class. They're
 *  organized roughly analogous to organization of other code in this program.
 *
 */
namespace X {
    // not currently used
    //class Multi : public Exception {
    //  private:
    //    std::vector<Exception *> elist;
    //
    //  public:
    //    size_t size() const;
    //
    //    void addException(Exception * e);
    //
    //    std::string what() override;
    //};

    /** \brief Exception for internal program errors.
     *
     *  This class is meant for errors indicating an internal error of some
     *  kind; e.g. an out-of-bounds check on input that should've been clamped
     *  beforehand.
     *
     *  The rule of thumb for using this class is "Is the user to blame for this
     *  error?". If the answer is "no", then use this.
     *
     */
    class InternalError : public Exception {
      private:
        std::string specifics; ///< Arbitrary string for error details.

      public:
        /** \brief Constructs an InternalError
         *
         *  As expected, constructs the internal error with a string giving
         *  arbitrary details on what happened.
         *
         *  \param[in] sp A string detailing what happened.
         *
         */
        InternalError(std::string sp);

        /// \brief Gives the error message (see Exception::what())
        std::string what() override;
    };

    /** \brief Exception for unimplemented features.
     *
     *  This class is only meant to be used in cases where a particular feature
     *  or aspect of it is not yet implemented (hence the abbreviation, NYI).
     *  This is mainly useful where a programmer doesn't wish to implement a
     *  less-common case for the time being.
     *
     *  \warning This is only meant as a \b temporary error; you are encouraged
     *           to replace errors of this type with intended functionality!
     *
     */
    class NYI : public Exception {
      private:
        std::string feature; ///< Description of NYI feature.

      public:
        /** \brief Constructs the NYI error
         *
         *  Takes an optional parameter specifying the feature. If not given
         *  then the phrase "this feature" is used.
         *
         *  \param[in] whatthing The name of the feature that's unimplemented.
         *             Used in a sentence like so: "[whatthing] is not yet
         *             implemented."
         *
         */
        NYI(std::string whatthing = "this feature");

        /// \brief Gives the error message (see Exception::what())
        std::string what() override;
    };

    /** \brief Exception when loading badly-formatted ROMs.
     *
     *  This class indicates that the ROM file you attempted to open is
     *  badly-formatted in some way. This error specifically <em>does not</em>
     *  indicate files that aren't ROMs after all; these errors come up on
     *  missing/inconsistent information when probing something we've determined
     *  is likely a ROM.
     *
     *  \todo Move to ROM sub-namespace.
     *
     */
    class BadROM : public Exception {
      private:
        std::string reason; ///< Explanation for the error.

      public:
        /** \brief Constructs the exception object.
         *
         *  \param[in] r The specific reason the ROM is bad.
         *
         */
        BadROM(std::string r);

        /// \brief Gives the error message (see Exception::what())
        std::string what() override;
    };

    /** \brief Exception for incorrect references into the ROM.
     *
     *  This class is for attempts to access files in the ROM with incorrect
     *  references, either start addresses that don't exist in the ROM's file
     *  table, or filenames that are undefined for the given ROM file.
     *
     *  \todo Move to ROM sub-namespace.
     *
     */
    class BadIndex : public Exception {
      private:
        std::string whatidx; ///< What kind of index turned out bad.

      public:
        /** \brief Constructs the exception.
         *
         *  \param[in] wi String giving the type of index that was attempted
         *                (virtual address, filename, etc.). Used in the form
         *                "Bad [wi] given."
         *
         */
        BadIndex(std::string wi);

        /// \brief Gives the error message (see Exception::what())
        std::string what() override;
    };

    /** \brief Exception indicating lack of configuration file.
     *
     *  This class is used whenever an operation needing a configuration file is
     *  attempted, but the ROM has no associated configuration file. While z64fe
     *  doesn't require a config file to use a ROM at all, quite a number of
     *  features will unsurprisingly require the use of config file info
     *  (usually to access particular files in a version-agnostic fashion).
     *
     *  \note This error only shows up if \em no configuration file was
     *        available. If the ROM has an available configuration file but not
     *        the necessary info, other errors occur.
     *
     *  \todo Move to Config sub-namespace.
     *
     */
    class NoConfig : public Exception {
      private:
        std::string trying; ///< What was attempted that needs config files.

      public:
        /** \brief Constructs the exception.
         *
         *  \param[in] t String describing the specific feature that needs
         *               configuration.
         *
         */
        NoConfig(std::string t);

        /// \brief Gives the error message (see Exception::what())
        std::string what() override;
    };

    /** \brief Namespace for ROM exceptions
     *
     *  This sub-namespace of the X namespace contains exceptions specific to
     *  ROM handling. Analogous to the ROM namespace in non-exception code.
     *
     */
    namespace ROM {
        /** \brief Exception indicating lack of magic word in ROM.
         *
         *  When initially scanning a file that might be a ROM, we look for a
         *  magic word that occurs a fixed number of bytes before the table of
         *  contents. If we can't find this magic word, you see this error.
         *
         *  \note This error can also pop up if your ROM file is interleaved in
         *        an unusual fashion; we currently only support un-interleaved
         *        and "byteswapped" files. If your file is
         *        otherwise-interleaved, you'll currently have to fix it
         *        beforehand.
         *
         *  Technical note: the magic word we look for is "zelda@", which may be
         *  "ezdl@a" in byteswapped ROMs. Otherwise-interleaved ROMs will have
         *  this magic word in a different format, possibly incorporating
         *  non-constant bytes.
         *
         */
        class NoMagic : public Exception {
          public:
            /// \brief Gives the error message (see Exception::what())
            std::string what() override;
        };
    }

    /** \brief Namespace for Config exceptions
     *
     *  This sub-namespace contains exceptions that get thrown by the
     *  configuration file handler when attempting to use it. This is analogous
     *  to the non-exception ::Config namespace.
     *
     */
    namespace Config {
        /** \brief Exception indicating syntax error in the chosen config file.
         *
         *  This generic error comes up if there's some kind of syntax error in
         *  the configuration file that the program attempted to load. It will
         *  of course provide a deeper explanation of the specifics.
         *
         */
        class SyntaxError : public Exception {
          private:
            std::string explain; ///< What went wrong, exactly.

          public:
            /** \brief Constructs the exception.
             *
             *  \param[in] e A string explaining what specifically went wrong.
             *
             */
            SyntaxError(std::string e);

            /// \brief Gives the error message (see Exception::what())
            std::string what() override;
        };

        /** \brief Exception indicating a bad character in the input file.
         *
         *  This is a subclass of SyntaxError. It's used when an invalid
         *  character is encountered in the parsing of a config file.
         *
         */
        class BadCharacter : public SyntaxError {
          private:
            char problemchar; ///< the problem character, as the name implies.

          public:
            /** \brief Constructs the exception.
             *
             *  \param[in] pc The bad character actually encountered.
             *
             */
            BadCharacter(char pc);

            // uses SyntaxError's what directly
        };

        /** \brief Exception indicating config property access failed.
         *
         *  This class generally indicates that a given key/value/etc. wasn't
         *  found in the search through the config data.
         *
         *  Subclasses are available for specific types of search failure.
         *  Prefer those over this class if possible.
         *
         */
        class NotFound : public Exception {
          private:
            std::string type;  ///< Type of data searched for
            std::string value; ///< Specific data searched for

          public:
            /** \brief Constructs the exception.
             *
             *  \param[in] t String identifying the type of data searched for
             *               (key/value/etc.)
             *
             *  \param[in] v String identifying the data you attempted to search
             *               for.
             *
             */
            NotFound(std::string t, std::string v);

            /// \brief Gives the error message (see Exception::what())
            std::string what() override;
        };

        /** \brief Exception indicating the given key was not found.
         *
         *  A subclass of X::Config::NotFound. Indicates the supplied key wasn't
         *  found in the search. This will come up on failed value lookups or a
         *  subkey lookup where a parent key doesn't exist.
         *
         *  This class can distinguish between a top-level (single-level) key
         *  and part of a multi-level key.
         *
         */
        class NoSuchKey : public NotFound {
          public:
            /** \brief Construct the exception.
             *
             *  Sets up the information for the parent class to report.
             *
             *  \param[in] key The key used in lookup.
             *
             *  \param[in] toplevel Optional. Boolean indicating if the given
             *                      key is a single-level key or not. Defaults
             *                      to \c false (part of multi-level key).
             *
             */
            NoSuchKey(std::string key, bool toplevel = false);
        };

        /** \brief Exception indicating the given value was not found.
         *
         *  A subclass of X::Config::NotFound. Indicates if the given value
         *  could not be found in the list. Used during reverse lookups (find
         *  key with given value) where the value was not found in the search
         *  location.
         *
         */
        class NoSuchValue : public NotFound {
          public:
            /** \brief Construct the exception.
             *
             *  \param[in] val The value searched for (and not found).
             *
             */
            NoSuchValue(std::string val);
        };
    }

    /** \brief Namespace for Yaz0 errors.
     *
     *  This sub-namespace is specifically for exception objects relating to
     *  Yaz0 handling.
     *
     */
    namespace Yaz0 {
        /** \brief Exception indicating an error in decompression.
         *
         *  This exception is a general one for decompression errors, which of
         *  course will explain what went wrong in more detail.
         *
         */
        class Decompress : public Exception {
          private:
            std::string reason; ///< The reason decompression failed.

          public:
            /** \brief Constructs the exception.
             *
             *  \param[in] r The reason decompression failed.
             *
             */
            Decompress(std::string r);

            /// \brief Gives the error message (see Exception::what())
            std::string what() override;
        };
    }

    /** \brief Namespace for in-game text erros.
     *
     *  This sub-namespace is designated for exceptions that come up when
     *  processing in-game text.
     *
     */
    namespace Text {
        /** \brief Exception indicating a bad sequence of bytes in text.
         *
         *  This class is thrown whenever an invalid character sequence occurs
         *  in the allegedly-text being processed.
         *
         */
        class BadSequence : public Exception {
          private:
            std::vector<uint8_t> badseq; ///< The problem sequence
            std::string optreason;       ///< Optional explanation of sequence.

          public:
            /** \brief Constructs the exception.
             *
             *  \param[in] bs The sequence of bytes that was deemed invalid.
             *                This parameter is an initializer list.
             *
             *  \param[in] optr Optional. A clarifying explanation of the bad
             *                  byte sequence, if applicable.
             *
             */
            BadSequence(std::initializer_list<uint8_t> bs, std::string optr = "");

            /// \brief Gives the error message (see Exception::what())
            std::string what() override;
        };

        /** \brief Exception indicating a bad text message header.
         *
         *  This class is thrown when the header accompanying a given message is
         *  malformed in some way.
         *
         */
        class HeaderError : public Exception {
          public:
            /// \brief Gives the error message (see Exception::what())
            std::string what() override;
        };
    }
}
/** \file
 *
 *  \brief Unit tests for the RCP::Vector class.
 *
 */

#include "RCP/Vector.hpp"

#include <QtTest/QtTest>

class UT_Vector : public QObject {
    Q_OBJECT

  private slots:
    /** \brief Tests RCP::Vector and as a byproduct ordinate functions.
     *
     *  Tests all the constructors defined for RCP::Vector. In testing these
     *  constructors work, we also end up testing the ordinate functions:
     *  RCP::Vector::x, RCP::Vector::y, and RCP::Vector::z.
     *
     */
    void ctorOrdinateTest() {
        RCP::Vector defctor;

        // QCOMPARE doesn't work with floating-point comparison to 0.0, due to
        // qFuzzyCompare
        QVERIFY2(defctor.x() == 0.0, "x ordinate isn't 0");
        QVERIFY2(defctor.y() == 0.0, "y ordinate isn't 0");
        QVERIFY2(defctor.z() == 0.0, "z ordinate isn't 0");

        RCP::Vector valctor(1.0, 2.125, -1.1);

        QCOMPARE(valctor.x(), 1.0);
        QCOMPARE(valctor.y(), 2.125);
        QCOMPARE(valctor.z(), -1.1);
    }

    /** \brief Tests the equality operators for RCP::Vector objects.
     */
    void equalTest() {
        QCOMPARE(RCP::Vector(1.0, 2.1, 3.3), RCP::Vector(1.0, 2.1, 3.3));
        QVERIFY2(RCP::Vector(0, 0, 0) != RCP::Vector(52.1, -1, 0.735), "Different vectors apparently equal?");
    }

    /** \brief Test arithmetic-assignment operators
     */
    void arithAssignTest() {
        RCP::Vector tv;

        tv += RCP::Vector(1.0, 2.0, 3.5);
        QCOMPARE(tv, RCP::Vector(1.0, 2.0, 3.5));

        tv -= RCP::Vector(0.5, 1.0, 2.5);
        QCOMPARE(tv, RCP::Vector(0.5, 1.0, 1.0));

        tv *= 2;
        QCOMPARE(tv, RCP::Vector(1.0, 2.0, 2.0));

        tv /= 4;
        QCOMPARE(tv, RCP::Vector(0.25, 0.5, 0.5));
    }

    /** \brief Test operators not easily grouped into other tests
     */
    void otherOpersTest() {
        QCOMPARE(-RCP::Vector(1.0, 1.0, -2.5), RCP::Vector(-1.0, -1.0, 2.5));

        QCOMPARE(RCP::Vector() + RCP::Vector(1, 2, 3), RCP::Vector(1, 2, 3));
        QCOMPARE(RCP::Vector() - RCP::Vector(-5, 0.25, 1), RCP::Vector(5, -0.25, -1));
        QCOMPARE(RCP::Vector(1,1,1) * 2, RCP::Vector(2,2,2));
        QCOMPARE(3 * RCP::Vector(2,3,0), RCP::Vector(6,9,0));
        QCOMPARE(RCP::Vector(6,4,2) / 2, RCP::Vector(3,2,1));
    }

    /** \brief Test RCP::Vector::length method
     */
    void lenTest() {
        QCOMPARE(RCP::Vector(1.0, 2.0, 0.0).length(), std::sqrt(5.0));
        QCOMPARE(RCP::Vector(0.0, -3.0, 0.0).length(), 3.0);
    }

    /** \brief Test dot product method
     */
    void dotTest() {
        QCOMPARE(RCP::Vector(1.0, 1.0, 1.0).dot(RCP::Vector(2.0, 0.0, 3.5)), 5.5);
        QCOMPARE(RCP::Vector(2.0, 3.0, 0.0).dot(RCP::Vector(1.5, -3.0, 1.0)), -6.0);

        RCP::Vector tvec(3.0, 1.5, -7.25);
        QCOMPARE(std::sqrt(tvec.dot(tvec)), tvec.length());
    }

    /** \brief Tests mutating transformation methods.
     */
    void mutatesTest() {
        RCP::Vector tvec(2.0, 0.0, 0.0);

        QCOMPARE(tvec.normalize(), RCP::Vector(1.0, 0.0, 0.0));
        QCOMPARE(tvec, RCP::Vector(1.0, 0.0, 0.0)); // check it actually mutated

        QCOMPARE(tvec.cross(RCP::Vector(0.0, 1.0, 0.0)), RCP::Vector(0.0, 0.0, 1.0));
        QCOMPARE(tvec, RCP::Vector(0.0, 0.0, 1.0)); // check it mutated
    }

    /** \brief Tests non-mutating transformation methods.
     */
    void nonmutatesTest() {
        RCP::Vector tvec(2.0, 0.0, 0.0);

        QCOMPARE(tvec.normalized(), RCP::Vector(1.0, 0.0, 0.0));
        QCOMPARE(tvec, RCP::Vector(2.0, 0.0, 0.0)); // check it didn't mutate

        QCOMPARE(tvec.crossed(RCP::Vector(0.0, 1.0, 0.0)), RCP::Vector(0.0, 0.0, 2.0));
        QCOMPARE(tvec, RCP::Vector(2.0, 0.0, 0.0)); // check it mutated
    }
};

QTEST_APPLESS_MAIN(UT_Vector)
#include "vectors.moc"
/** \file
 *
 *  \brief Test file for the RCP Matrix class
 *
 */

#include "RCP/Matrix.hpp"

#include <QtTest/QtTest>

#include <iostream>

class UT_Matrix : public QObject {
    Q_OBJECT

  private slots:
    /** \brief Test RCP::Matrix constructors (and raw data method as byproduct)
     *
     *  This test is for all RCP::Matrix constructors. In order to verify these
     *  constructors Do The Right Thing™, we end up relying on (and thus
     *  implicitly testing) RCP::Matrix::rawRowMajor working.
     *
     */
    void ctorTest() {
        std::array<double, 16> verifying;

        std::array<double, 16> standard;

        verifying = RCP::Matrix().rawRowMajor();
        standard.fill(0);
        QCOMPARE(verifying, standard);

        verifying = RCP::Matrix(1).rawRowMajor();
        standard[0] = standard[5] = standard[10] = standard[15] = 1.0;
        QCOMPARE(verifying, standard);

        verifying = RCP::Matrix(-5).rawRowMajor();
        standard[0] = standard[5] = standard[10] = standard[15] = -5.0;
        QCOMPARE(verifying, standard);

        // note the use of binary-friendly fractional parts, just to make sure
        // we don't fail on lack of fuzzy matching.
        verifying = RCP::Matrix{1.25,  2.5,   0.0,    -1.5,
                                57.0, -1.0,   9.125,   3.0,
                                1.75, -7.5,  -0.0625,  0.0,
                                0.5,   30.0,  1.25,    0.0}.rawRowMajor();

        standard = {1.25,  2.5,   0.0,    -1.5,
                    57.0, -1.0,   9.125,   3.0,
                    1.75, -7.5,  -0.0625,  0.0,
                    0.5,   30.0,  1.25,    0.0};

        QCOMPARE(verifying, standard);
    }

    /** \brief Test both raw data getters
     */
    void rawTest() {
        RCP::Matrix themtx{ 0,  1,  2,  3,
                            4,  5,  6,  7,
                            8,  9, 10, 11,
                           12, 13, 14, 15};

        std::array<double, 16> rawform { 0,  1,  2,  3,
                                         4,  5,  6,  7,
                                         8,  9, 10, 11,
                                        12, 13, 14, 15};

        QCOMPARE(themtx.rawRowMajor(), rawform);

        rawform = {0,  4,  8, 12,
                   1,  5,  9, 13,
                   2,  6, 10, 14,
                   3,  7, 11, 15};

        QCOMPARE(themtx.rawColumnMajor(), rawform);
    }

    /** \brief Test supplied iterators
     */
    void iterTest() {
        RCP::Matrix themtx(3);
        std::array<double, 16> cpmtx;

        auto j = cpmtx.begin();

        for (auto & i : themtx) {
            *j++ = i;
        }

        QCOMPARE(cpmtx, themtx.rawRowMajor());

        QCOMPARE(std::distance(themtx.begin(), themtx.end()), ptrdiff_t(16));
    }

    /** \brief Test element access methods/operators
     */
    void elementTest() {
        RCP::Matrix themtx(1);
        const RCP::Matrix cmtx(1);

        QCOMPARE(themtx.at(0,0), 1.0);

        themtx.at(1,3) = 5.5;
        QCOMPARE(themtx.at(1,3), 5.5);

        QCOMPARE(cmtx.at(2,2), 1.0);
        QVERIFY(!std::is_lvalue_reference<decltype(cmtx.at(2,2))>::value);

        // now to test operator[]
        QCOMPARE(themtx[1][2], 0.0);

        themtx[0][0] = -1.5;
        QCOMPARE(themtx[0][0], -1.5);

        QCOMPARE(cmtx[1][1], 1.0);
        QVERIFY(!std::is_lvalue_reference<decltype(cmtx[1][1])>::value);
    }

    /** \brief Test static matrix generation functions
     */
    void staticTest() {
        QCOMPARE(RCP::Matrix::identity(), RCP::Matrix(1));

        RCP::Matrix testmtx = RCP::Matrix::orthographic(RCP::Point{0.0, 0.0, 0.0}, RCP::Point{8.0, 16.0, 0.0}, 64.0, 128.0);


        RCP::Matrix stdmtx{
             0.25,  0.0,     0.0,      0.0,
             0.0,   0.125,   0.0,      0.0,
             0.0,   0.0,    -0.03125,  0.0,
            -1.0,  -1.0,    -3.0,      1.0};

        QCOMPARE(testmtx, stdmtx);

        testmtx = RCP::Matrix::perspective(std::acos(-1) / 4.0, 4.0 / 3.0, 32.0, 64.0);
        stdmtx = RCP::Matrix{
            1.810660,  0.0,        0.0,     0.0,
            0.0,       2.414213,   0.0,     0.0,
            0.0,       0.0,       -3.0,    -1.0,
            0.0,       0.0,       -128.0,   0.0};

        QCOMPARE(testmtx, stdmtx);

        testmtx = RCP::Matrix::frustum(RCP::Point{2.0, 2.0, 0.0}, RCP::Point{10.0, 10.0, 0.0}, 0.1, 16.0);
        stdmtx = RCP::Matrix{
            0.025,  0.0,     0.0,        0.0,
            0.0,    0.025,   0.0,        0.0,
            1.5,    1.5,    -1.012579,  -1.0,
            0.0,    0.0,    -0.201258,   0.0};

        QCOMPARE(testmtx, stdmtx);

        testmtx = RCP::Matrix::lookAt(RCP::Vector(-2.0,10.0,-5.0), RCP::Vector(0.0, 0.0, 0.0),
                                      RCP::Vector(0.0, 0.0, 1.0));
        stdmtx = RCP::Matrix{
            -0.980581,  -0.086335,  -0.176090,   0.0,
            -0.196116,   0.431677,   0.880451,   0.0,
             0.0,        0.897887,  -0.440225,   0.0,
             0.0,        0.0,       -11.357817,  1.0};



        QCOMPARE(testmtx, stdmtx);
    }

    /** \brief Test arithmetic-assign operators
     */
    void arithAssignTest() {
        RCP::Matrix tst(1);

        tst += RCP::Matrix(2);
        QCOMPARE(tst, RCP::Matrix(3));

        tst -= RCP::Matrix(1);
        QCOMPARE(tst, RCP::Matrix(2));

        tst *= 2.0;
        QCOMPARE(tst, RCP::Matrix(4));

        tst /= 4.0;
        QCOMPARE(tst, RCP::Matrix(1));

        // now to test matrix multiply
        tst = RCP::Matrix{
             0.0,  1.0,  2.0,  3.0,
             4.0,  5.0,  6.0,  7.0,
             8.0,  9.0, 10.0, 11.0,
            12.0, 13.0, 14.0, 15.0};

        RCP::Matrix res{
             56,  62,  68,  74,
            152, 174, 196, 218,
            248, 286, 324, 362,
            344, 398, 452, 506};

        QCOMPARE(tst * tst, res);
    }

    /** \brief Test determinant function
     */
    void detTest() {
        QCOMPARE(RCP::Matrix::identity().det(), 1.0);
    }
};

QTEST_APPLESS_MAIN(UT_Matrix)
#include "matrices.moc"
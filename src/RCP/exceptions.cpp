/** \file
 *
 *  \brief Implement RCP exceptions
 *
 */

#include "RCP/exceptions.hpp"

#include <sstream>
#include <iomanip>

namespace X {
    namespace RCP {
        namespace Matrix {
            std::string CantInvert::what() {
                return "Can't invert this matrix :(";
            }
        }

        BadSegment::BadSegment(uint32_t bs) : badseg(bs) { }

        std::string BadSegment::what() {
            std::stringstream msg;

            msg << "Got bad RCP segment address 0x"
                << std::hex << std::setfill('0') << std::uppercase
                << std::setw(2) << badseg
                << ". This is either not implemented or holds a currently unknown meaning.";

            return msg.str();
        }

        BadOtherMode::BadOtherMode(std::string h, uint8_t gsft, uint8_t gsiz, uint32_t gval)
            : half(h),
              given_sft(gsft),
              given_siz(gsiz),
              given_val(gval) { }

        std::string BadOtherMode::what() {
            std::stringstream msg;

            msg << std::hex << std::setfill('0') << std::uppercase;

            msg << "Bad mode setting for " << half << " half of other mode"
                << ": Shift 0x" << std::setw(2) << +given_sft
                << ", Size 0x" << std::setw(2) << +given_siz
                << ", Value 0x" << std::setw(8) << given_val;

            return msg.str();
        }

        namespace TMEM {
            OutOfRange::OutOfRange(size_t bidx, size_t bper, std::string wh) : where(wh),
                                                                               badidx(bidx),
                                                                               bitsper(bper) { }

            std::string OutOfRange::what() {
                std::stringstream msg;

                msg << std::hex << std::setfill('0') << std::uppercase;

                switch (bitsper) {
                  case 8:
                    msg << "Byte";
                    break;

                  case 64:
                    msg << "Word";
                    break;

                  default:
                    msg << std::dec << bitsper << std::hex << "-bit";
                }

                msg << " index 0x" << bidx << " invalid for ";

                if (where) {
                    msg << where << " ";
                }

                msg << "TMEM";

                return msg.str();
            }

            OutOfRangeLow::OutOfRangeLow(size_t bidx, size_t bper) : OutOfRange(bidx, bper,
                                                                                "lower half of") { }

            OutOfRangeHigh::OutOfRangeHigh(size_t bidx, size_t bper) : OutOfRange(bidx, bper,
                                                                                  "higher half of") { }
        }
    }
}
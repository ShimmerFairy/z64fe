/** \file
 *
 *  \brief Implement various member functions as needed for TMEM stuff.
 *
 */

#include "RCP/Tile.hpp"

namespace RCP {
    void TMEM::inflip() { interflag ^= true; }
    void TMEM::inreset() { interflag = false; }

    uint8_t & TMEM::mlo(size_t idx) {
        if (idx >= 2048) {
            throw X::RCP::TMEM::OutOfRangeLow(idx);
        }

        // (theoretically) faster route for when we don't have to worry about
        // interleaving
        if (!interflag) {
            return mem[idx];
        }

        if (idx % 8 < 4) {
            return mem[idx + 4];
        } else {
            return mem[idx - 4];
        }
    }

    uint8_t & TMEM::mhi(size_t idx) {
        if (idx >= 2048) {
            throw X::RCP::TMEM::OutOfRangeHigh(idx);
        }

        if (!interflag) {
            return mem[idx + 2048];
        }

        if (idx % 8 < 4) {
            return mem[idx + 2048 + 4];
        } else {
            return mem[idx + 2048 - 4];
        }
    }

    uint8_t & TMEM::mat(size_t idx) {
        if (idx >= 4096) {
            throw X::RCP::TMEM::OutOfRange(idx);
        }

        if (!interflag) {
            return mem[idx];
        }

        if (idx % 8 < 4) {
            return mem[idx + 4];
        } else {
            return mem[idx - 4];
        }
    }
}
/** \file
 *
 *  \brief Implement the Matrix class
 *
 */

#include "RCP/Matrix.hpp"
#include "RCP/exceptions.hpp"

#include <cmath>
#include <algorithm>
#include <iostream>
#include <iomanip>

namespace RCP {
    Matrix::MRow::MRow(Matrix & mref, size_t rnum) : ref(mref), row(rnum) { }

    double & Matrix::MRow::operator[](size_t col) {
        return ref.parts[row * 4 + col];
    }

    Matrix::ConstMRow::ConstMRow(const Matrix & mref, size_t rnum) : ref(mref), row(rnum) { }

    double Matrix::ConstMRow::operator[](size_t col) const {
        return ref.parts[row * 4 + col];
    }

    void Matrix::ip_recurse(std::vector<std::vector<size_t>> & ref, std::vector<size_t> prefix) {
        if (prefix.size() == 4) {
            ref.push_back(prefix);
        } else {
            for (size_t i = 0; i < 4; i++) {
                if (std::find(prefix.begin(), prefix.end(), i) != prefix.end()) {
                    continue;
                }

                std::vector<size_t> pp = prefix;
                pp.push_back(i);

                ip_recurse(ref, pp);
            }
        }
    }

    std::vector<std::vector<size_t>> Matrix::idx_perm() {
        std::vector<std::vector<size_t>> res;

        for (size_t i = 0; i < 4; i++) {
            ip_recurse(res, std::vector<size_t>(i));
        }

        return res;
    }

    double Matrix::sgn(std::vector<size_t> vec) {
        // for each element, add up the number of items greater than it before
        // it

        size_t sum = 0;

        // start from index 1 since index 0 is trivially always zero for the
        // inversion count
        for (size_t i = 1; i < vec.size(); i++) {
            sum += std::count_if(vec.begin(), vec.begin() + 1,
                                 [&](const size_t & a) {
                                     return a > vec[i];
                                 });
        }

        return std::pow(-1.0, sum);
    }

    Matrix::Matrix() {
        parts.fill(0);
    }

    Matrix::Matrix(double scale) {
        parts.fill(0);
        at(0,0) = at(1,1) = at(2,2) = at(3,3) = scale;
    }

    Matrix::Matrix(std::initializer_list<double> nparts) {
        for (size_t i = 0; i < 16; i++) {
            parts[i] = *(nparts.begin() + i);
        }
    }

    std::array<double, 16> Matrix::rawRowMajor() const {
        return parts;
    }

    std::array<double, 16> Matrix::rawColumnMajor() const {
        return transposition().parts;
    }

    double & Matrix::at(size_t row, size_t col) {
        return parts.at(row * 4 + col);
    }

    double Matrix::at(size_t row, size_t col) const {
        return parts.at(row * 4 + col);
    }

    auto Matrix::operator[](size_t row) -> MRow {
        return MRow(*this, row);
    }

    auto Matrix::operator[](size_t row) const -> ConstMRow {
        return ConstMRow(*this, row);
    }

    auto Matrix::begin() -> decltype(parts)::iterator {
        return parts.begin();
    }

    auto Matrix::end() -> decltype(parts)::iterator {
        return parts.end();
    }

    Matrix Matrix::identity() {
        return Matrix(1);
    }

    Matrix Matrix::orthographic(Point ll, Point ur, double nearZ, double farZ) {
        const double xdiff = ur.x - ll.x;
        const double ydiff = ur.y - ll.y;
        const double zdiff = farZ - nearZ;
        const double xsum  = ur.x + ll.x;
        const double ysum  = ur.y + ll.y;
        const double zsum  = farZ + nearZ;

        return Matrix{
             2.0 / xdiff,    0.0,            0.0,           0.0,
             0.0,            2.0 / ydiff,    0.0,           0.0,
             0.0,            0.0,           -2.0 / zdiff,   0.0,
            -xsum / xdiff,  -ysum / ydiff,  -zsum / zdiff,  1.0
        };
    }

    Matrix Matrix::perspective(double yfov, double aspect, double nearZ, double farZ) {
        const double cot = std::cos(yfov / 2.0) / std::sin(yfov / 2.0);

        return Matrix{
            cot / aspect,  0.0,  0.0,                                0.0,
            0.0,           cot,  0.0,                                0.0,
            0.0,           0.0,  (nearZ + farZ) / (nearZ - farZ),   -1.0,
            0.0,           0.0,  2 * nearZ * farZ / (nearZ - farZ),  0.0,
        };
    }

    Matrix Matrix::frustum(Point ll, Point ur, double nearZ, double farZ) {
        const double xdiff = ur.x - ll.x;
        const double ydiff = ur.y - ll.y;
        const double zdiff = farZ - nearZ;
        const double xsum  = ur.x + ll.x;
        const double ysum  = ur.y + ll.y;
        const double zsum  = farZ + nearZ;

        return Matrix{
            2.0 * nearZ / xdiff,  0.0,                0.0,                          0.0,
            0.0,                  2 * nearZ / ydiff,  0.0,                          0.0,
            xsum / xdiff,         ysum / ydiff,       -zsum / zdiff,               -1.0,
            0.0,                  0.0,                -2.0 * farZ * nearZ / zdiff,  0.0,
        };
    }

    Matrix Matrix::lookAt(Vector eye, Vector seeing, Vector up) {
        // first step, get the eye->seeing vector

        Vector looking = eye - seeing; // modern ancient proverb
        looking.normalize();

        // second of all, find the right-pointing arrow based on up and looking
        // (aka forward)

        Vector right = up.crossed(looking).normalized();

        // now to find the real up vector (since the one given to us doesn't
        // necessarily have to be the right up vector to be useful in getting
        // the previous vector(?)).

        Vector realUp = looking.crossed(right);

        return Matrix::identity().translation(-eye) * Matrix{
            right.x(),           realUp.x(),        looking.x(),          0,
            right.y(),           realUp.y(),        looking.y(),          0,
            right.z(),           realUp.z(),        looking.z(),          0,
            0,                   0,                 0,                    1,
        };
    }

    Matrix & Matrix::operator+=(const Matrix & that) {
        std::array<double, 16> res;

        std::transform(parts.begin(), parts.end(),
                       that.parts.begin(),
                       res.begin(),
                       [](const double & a, const double & b) {
                           return a + b;
                       });

        parts = res;

        return *this;
    }

    Matrix & Matrix::operator-=(const Matrix & that) {
        std::array<double, 16> res;

        std::transform(parts.begin(), parts.end(),
                       that.parts.begin(),
                       res.begin(),
                       [](const double & a, const double & b) {
                           return a - b;
                       });

        parts = res;

        return *this;
    }

    Matrix & Matrix::operator*=(const Matrix & that) {
        Matrix res;

        for (size_t row = 0; row < 4; row++) {
            for (size_t col = 0; col < 4; col++) {
                for (size_t i = 0; i < 4; i++) {
                    res[row][col] += (*this)[row][i] * that[i][col];
                }
            }
        }

        parts = res.parts;

        return *this;
    }

    Matrix & Matrix::operator*=(const double & that) {
        for (auto & i : parts) {
            i *= that;
        }

        return *this;
    }

    Matrix & Matrix::operator/=(const double & that) {
        for (auto & i : parts) {
            i /= that;
        }

        return *this;
    }

    double Matrix::det() {
        double res = 0.0;

        std::vector<std::vector<size_t>> perms = idx_perm();

        for (auto & p : perms) {
            double prod = sgn(p);

            for (size_t i = 0; i < 4; i++) {
                prod *= at(i, p[i]);
            }

            res += prod;
        }

        return res;
    }

    bool Matrix::singular() {
        return det() == 0;
    }

    Matrix & Matrix::transpose() {
        std::array<double, 16> res;

        for (size_t row = 0; row < 4; row++) {
            for (size_t col = 0; col < 4; col++) {
                res[row * 4 + col] = parts[col * 4 + row];
            }
        }

        parts = res;

        return *this;
    }

    Matrix & Matrix::invert() {
        if (singular()) {
            throw X::RCP::Matrix::CantInvert();
        }

        // we'll be doing Gauss-Jordan elimination to get the inverse, but with
        // pivoting it'll be a simple operation.

        Matrix id_to_inv = identity(); // the 'second matrix' in use here.

        // loop over each row as it's used to pivot (we'll always pivot on the
        // diagonals for ease of implementation)
        for (size_t i = 0; i < 4; i++) {
            std::array<double, 8> row;

            std::copy(begin() + i * 4, begin() + i * 4 + 4,
                      row.begin());
            std::copy(id_to_inv.begin() + i * 4, id_to_inv.begin() + i * 4 + 4,
                      row.begin() + 4);

            // loop over the other rows so we can affect them properly
            for (size_t j = 0; j < 4; j++) {
                if (i == j) {
                    continue;
                }

                double rowfac = parts[j*4 + i];
                double otherfac = row[i];

                std::array<double, 8> rowcp;

                std::transform(row.begin(), row.end(),
                               rowcp.begin(),
                               [&rowfac](const double & a) { return rowfac * a; });

                // loop over each element in our imaginary [4][8] matrix to
                // subtract the multiplied pivot row from the current target row
                // (j).
                for (size_t k = 0; k < 4; k++) {
                    at(j, k) *= otherfac;
                    at(j, k) -= rowcp[k];
                    id_to_inv.at(j, k) *= otherfac;
                    id_to_inv.at(j, k) -= rowcp[k + 4];
                }
            }
        }

        // now that we're done pivoting, we just want to make sure we can reduce
        // the left-hand side if possible. Since we know we want the LHS (this)
        // to be an identity matrix, we know which factor to divide everything
        // by if we're to have a hope of a chance of an invertible matrix.
        for (size_t i = 0; i < 4; i++) {
            double divisor = at(i, i);

            for (size_t j = 0; j < 4; j++) {
                at(i, j) /= divisor;
                id_to_inv.at(i, j) /= divisor;
            }
        }

        // and now, check if we're the identity matrix. If so, the other half's
        // got the inverse. If not, that's a problem.
        if (*this != identity()) {
            throw X::RCP::Matrix::CantInvert();
        }

        *this = id_to_inv;

        return *this;
    }

    Matrix & Matrix::rotate(double rad, Vector around) {
        const double cosamt = std::cos(rad);
        const double sinamt = std::sin(rad);
        const double _1mcosamt = 1 - cosamt;

        Vector ax = around.normalized();

        Matrix rotm {
             cosamt,            ax.z() * sinamt,  -ax.y() * sinamt,  0,
            -ax.z() * sinamt,   cosamt,            ax.x() * sinamt,  0,
             ax.y() * sinamt,  -ax.x() * sinamt,   cosamt,           0,
             0,                 0,                 0,                1
        };

        rotm += _1mcosamt * Matrix {
            ax.x() * ax.x(),  ax.x() * ax.y(),  ax.x() * ax.z(), 0,
            ax.y() * ax.x(),  ax.y() * ax.y(),  ax.y() * ax.z(), 0,
            ax.z() * ax.x(),  ax.z() * ax.y(),  ax.z() * ax.z(), 0,
            0,                0,                0,               0,
        };

        *this *= rotm;

        return *this;
    }

    Matrix & Matrix::translate(Vector dir) {
        Matrix transm {
            1.0,     0.0,     0.0,     0.0,
            0.0,     1.0,     0.0,     0.0,
            0.0,     0.0,     1.0,     0.0,
            dir.x(), dir.y(), dir.z(), 1.0,
        };

        *this *= transm;

        return *this;
    }

    Matrix & Matrix::scale(Vector factor) {
        Matrix scalem {
            factor.x(),  0,           0,           0,
            0,           factor.y(),  0,           0,
            0,           0,           factor.z(),  0,
            0,           0,           0,           1,
        };

        *this *= scalem;

        return *this;
    }

    Matrix Matrix::transposition() const {
        Matrix res = *this;
        return res.transpose();
    }

    Matrix Matrix::inverse() const {
        Matrix res = *this;
        return res.invert();
    }

    Matrix Matrix::rotation(double rad, Vector around) const {
        Matrix res = *this;
        return res.rotate(rad, around);
    }

    Matrix Matrix::translation(Vector dir) const {
        Matrix res = *this;
        return res.translate(dir);
    }

    Matrix Matrix::scaled(Vector factor) const {
        Matrix res = *this;
        return res.scale(factor);
    }

    Matrix operator+(const Matrix & a, const Matrix & b) {
        Matrix res = a;
        res += b;
        return res;
    }

    Matrix operator-(const Matrix & a, const Matrix & b) {
        Matrix res = a;
        res -= b;
        return res;
    }

    Matrix operator*(const Matrix & a, const Matrix & b) {
        Matrix res = a;
        res *= b;
        return res;
    }

    Matrix operator*(const Matrix & a, const double & b) {
        Matrix res = a;
        res *= b;
        return res;
    }

    Vector operator*(const Matrix & a, const Vector & b) {
        Matrix newb(0);
        newb[0][0] = b.x();
        newb[1][0] = b.y();
        newb[2][0] = b.z();
        newb[3][0] = 1.0;

        newb = a * newb;

        return Vector(newb[0][0] / newb[3][0], newb[1][0] / newb[3][0], newb[2][0] / newb[3][0]);
    }
        

    Vector operator*(const Vector & a, const Matrix & b) {
        Matrix newa(0);
        newa[0][0] = a.x();
        newa[0][1] = a.y();
        newa[0][2] = a.z();
        newa[0][3] = 1.0;

        newa = newa * b;

        return Vector(newa[0][0] / newa[0][3], newa[0][1] / newa[0][3], newa[0][2] / newa[0][3]);
    }

    Vector & operator*=(Vector & a, const Matrix & b) {
        a = a * b;
        return a;
    }

    Matrix operator*(const double & a, const Matrix & b) {
        Matrix res = b;
        res *= a;
        return res;
    }

    Matrix operator/(const Matrix & a, const double & b) {
        Matrix res = a;
        res /= b;
        return res;
    }

    bool operator==(const Matrix & a, const Matrix & b) {
        auto arrm = a.rawRowMajor();
        auto brrm = b.rawRowMajor();

        for (size_t i = 0; i < 16; i++) {
            if (arrm[i] == brrm[i]) {
                continue;
            } else if (std::signbit(arrm[i]) != std::signbit(brrm[i])) {
                return false;
            }

            double delta = std::abs(arrm[i] - brrm[i]);
            if (delta >= 1.0e-6) {
                return false;
            }
        }

        return true;
    }

    bool operator!=(const Matrix & a, const Matrix & b) {
        return !(a == b);
    }

    std::ostream & operator<<(std::ostream & os, const Matrix & mtx) {
        // start off with a newline for alignment purposes (if we could find the
        // initial offset on the line, this wouldn't be necessary)
        os << "\n";
        size_t prec = os.precision();
        os << std::fixed << std::showpos << std::setprecision(6);

        // now the matrix
        for (size_t i = 0; i < 4; i++) {
            os << "[\t";
            for (size_t j = 0; j < 4; j++) {
                os << mtx[i][j] << "\t";
            }
            os << "]\n";
        }

        os.precision(prec);
        os << std::defaultfloat << std::noshowpos;

        return os;
    }
}
/** \file
 *
 *  \brief Implement vectors in RCP
 *
 */

#include "RCP/Vector.hpp"

#include <algorithm>
#include <cmath>

namespace RCP {
    Vector::Vector() {
        pointTo.fill(0);
    }

    Vector::Vector(double X, double Y, double Z) {
        pointTo = { X, Y, Z };
    }

    Vector & Vector::operator+=(const Vector & that) {
        std::array<double, 3> res;

        std::transform(pointTo.begin(), pointTo.end(),
                       that.pointTo.begin(),
                       res.begin(),
                       [](const double & a, const double & b) { return a + b; });

        pointTo = res;
        return *this;
    }

    Vector & Vector::operator-=(const Vector & that) {
        std::array<double, 3> res;

        std::transform(pointTo.begin(), pointTo.end(),
                       that.pointTo.begin(),
                       res.begin(),
                       [](const double & a, const double & b) { return a - b; });

        pointTo = res;
        return *this;
    }

    Vector & Vector::operator*=(const double & scale) {
        for (auto & i : pointTo) {
            i *= scale;
        }

        return *this;
    }

    Vector & Vector::operator/=(const double & scale) {
        for (auto & i : pointTo) {
            i /= scale;
        }

        return *this;
    }

    Vector Vector::operator-() const {
        Vector res = *this;
        for (auto & i : res.pointTo) {
            i *= -1;
        }

        return res;
    }

    bool Vector::operator==(const Vector & that) const {
        return pointTo == that.pointTo;
    }

    bool Vector::operator!=(const Vector & that) const {
        return !(*this == that);
    }

    double Vector::length() const {
        return std::sqrt(dot(*this));
    }

    double Vector::x() const { return pointTo[0]; }
    double Vector::y() const { return pointTo[1]; }
    double Vector::z() const { return pointTo[2]; }

    double & Vector::x() { return pointTo[0]; }
    double & Vector::y() { return pointTo[1]; }
    double & Vector::z() { return pointTo[2]; }

    std::array<double, 3>::iterator Vector::begin() { return pointTo.begin(); }
    std::array<double, 3>::iterator Vector::end() { return pointTo.end(); }

    std::array<double, 3>::const_iterator Vector::begin() const { return pointTo.begin(); }
    std::array<double, 3>::const_iterator Vector::end() const { return pointTo.end(); }

    double Vector::dot(const Vector & that) const {
        return std::inner_product(
            pointTo.begin(), pointTo.end(),
            that.pointTo.begin(),
            0.0);
    }

    Vector & Vector::normalize() {
        *this /= length();
        return *this;
    }

    Vector & Vector::cross(const Vector & that) {
        std::array<double, 3> oldpt = pointTo;

        pointTo[0] = oldpt[1] * that.pointTo[2] - oldpt[2] * that.pointTo[1];
        pointTo[1] = oldpt[2] * that.pointTo[0] - oldpt[0] * that.pointTo[2];
        pointTo[2] = oldpt[0] * that.pointTo[1] - oldpt[1] * that.pointTo[0];

        return *this;
    }

    Vector Vector::normalized() const {
        Vector res = *this;
        return res.normalize();
    }

    Vector Vector::crossed(const Vector & that) const {
        Vector res = *this;
        return res.cross(that);
    }

    Vector operator+(const Vector & a, const Vector & b) {
        Vector res = a;
        res += b;
        return res;
    }

    Vector operator-(const Vector & a, const Vector & b) {
        Vector res = a;
        res -= b;
        return res;
    }

    Vector operator*(const Vector & a, const double & b) {
        Vector res = a;
        res *= b;
        return res;
    }

    Vector operator*(const double & a, const Vector & b) {
        Vector res = b;
        res *= a;
        return res;
    }

    Vector operator/(const Vector & a, const double & b) {
        Vector res = a;
        res /= b;
        return res;
    }

    std::ostream & operator<<(std::ostream & os, const Vector & vec) {
        os << "( "
           << vec.x() << " "
           << vec.y() << " "
           << vec.z() << " )";

        return os;
    }
}
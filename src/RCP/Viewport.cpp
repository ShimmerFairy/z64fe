/** \file
 *
 *  \brief Implement the viewport
 *
 */

#include "RCP/Viewport.hpp"
#include "RCP/exceptions.hpp"

#include <QtDebug>
#include <QKeyEvent>
#include <QMessageBox>

#include <iostream>
#include <unordered_set>

namespace RCP {
    Viewport::Viewport(ROM::ROM * tr, ROM::File objf, QWidget * parent) : QOpenGLWidget(parent), 
                                                                          therom(tr),
                                                                          objfile(objf) { }

    ROM::File Viewport::segmentFile(uint8_t seg) {
        switch (seg) {
          case 0x01:
            return therom->fileAtName("nintendo_rogo_static");
            break;

          case 0x02:
            // current scene, NYI
            throw X::RCP::BadSegment(seg);
            break;

          case 0x03:
            // current room, NYI
            throw X::RCP::BadSegment(seg);
            break;

          case 0x04:
            return therom->fileAtName("gameplay_keep");
            break;

          case 0x05:
            switch (Config::getGame(therom->getVersion())) {
              case Config::Game::Ocarina:
                return therom->fileAtName("gameplay_field_keep");
                break;

              case Config::Game::Majora:
                // field filename misspelled in MM
                return therom->fileAtName("gameplay_feild_keep");
                break;

              default:
                throw X::InternalError("Got an unknown game specifier from ROM version.");
                break;
            }
            break;

          case 0x06:
            // the object file itself
            return objfile;
            break;

          case 0x07:
            // either unknown or link's animations, assume animations
            return therom->fileAtName("link_animetion");
            break;

          default:
            throw X::RCP::BadSegment(seg);
            break;
        }
    }

    void Viewport::checkshd(GLuint sid) {
        GLint status;
        glGetShaderiv(sid, GL_COMPILE_STATUS, &status);

        if (status != GL_TRUE) {
            char tbuf[512];
            glGetShaderInfoLog(sid, 512, nullptr, tbuf);

            qDebug() << "OH NO!:" << tbuf;
        }
    }

    void Viewport::changeDL(DisplayList nobj) {
        renderThis = nobj;
        cando = true;
        state_gm = GeometryMode();
        state_om = OtherMode();
        state_cc_1 = CCCycle();
        state_cc_2 = CCCycle();

        vtx_preGL.fill(Vertex());
        vtx_postGL.fill(GLVertex());
        thecam = Camera();
        thecam.setAspect(width(), height());

        // first figure out the proper zoom from the final product, by
        // collecting vertices as they're used, and then use that to figure out
        // the center of the object and its bounding box. We do this so the
        // viewer always starts by showing you the whole model, instead of a
        // blank space you hope has the model, just out of bounds.
        std::array<Vertex, 32> fakevtx;
        std::unordered_set<Vector> vtxused;

        try {
            for (auto & i : renderThis) {
                if (command_cast<Command::VTX *>(i)) {
                    Command::VTX * c_vtx = command_cast<Command::VTX *>(i);

                    ROM::File sf = segmentFile(c_vtx->ram_address >> 24);

                    auto fptr = sf.begin() + (c_vtx->ram_address & 0x00FFFFFF);

                    for (size_t j = 0; j < c_vtx->size; j++) {
                        RawVertex rv;
                        rv.position[0] = be_s16(fptr); fptr += 2;
                        rv.position[1] = be_s16(fptr); fptr += 2;
                        rv.position[2] = be_s16(fptr); fptr += 2;
                        fptr += 2;
                        rv.tex_pos[0] = be_s16(fptr); fptr += 2;
                        rv.tex_pos[1] = be_s16(fptr); fptr += 2;

                        // for now, always assume lighting is off
                        rv.dummy_normflag = 0;
                        rv.RGB[0] = *fptr++;
                        rv.RGB[1] = *fptr++;
                        rv.RGB[2] = *fptr++;

                        rv.alpha = *fptr++;

                        fakevtx[c_vtx->dst_idx + j] = Vertex(rv);
                    }
                } else if (command_cast<Command::ENDDL *>(i)) {
                    break;
                } else if (command_cast<Command::TRI1 *>(i)) {
                    for (auto & j : command_cast<Command::TRI1 *>(i)->vtx_idxs) {
                        vtxused.insert(fakevtx[j].position);
                    }
                } else if (command_cast<Command::TRI2 *>(i)) {
                    for (auto & j : command_cast<Command::TRI2 *>(i)->vtx_idxs) {
                        vtxused.insert(fakevtx[j].position);
                    }
                }
            }
        } catch (Exception & e) {
            cando = false;
            QMessageBox::critical(this, tr("Rendering Error"), e.what().c_str());
            return;
        }

        const double inf = std::numeric_limits<double>::infinity();
        Vector bb_mins(inf, inf, inf);
        Vector bb_maxs(-inf, -inf, -inf);

        for (auto & i : vtxused) {
            thecam.focus += i;
            auto bmin = bb_mins.begin();
            auto bmax = bb_maxs.begin();
            for (auto & j : i) { // note: gcc 5.3.0 insists i.position
                                          // is a const object, even though I
                                          // have no clue how the fuck gcc
                                          // thinks this.
                if (j < *bmin) {
                    *bmin = j;
                }

                if (j > *bmax) {
                    *bmax = j;
                }
                bmin++;
                bmax++;
            }
        }

        // just divide the center by the number of vertices, and we have the
        // center we need.
        thecam.focus /= vtxused.size();

        // the bounding box is similarly simple, we just have to adjust them so
        // they're distances from the object center, not distances from the
        // origin.

        bb_mins -= thecam.focus;
        bb_maxs -= thecam.focus;

        // we'll figure out a good enough distance by taking the sum of the
        // distances of the two corners as the amount to translate away from.

        thecam.pos = thecam.focus - Vector(0.0, 0.0, bb_maxs.length() + bb_mins.length());

        bb_mins.z() = thecam.pos.z();

        thecam.setDepthRange(bb_mins.z(), bb_maxs.z());

        std::clog << std::string(80, '=') << "\n";
        std::clog << "VTX:::\n";

        for (auto & i : vtxused) {
            std::clog << "    " << i << "\n";
        }

        std::clog << "MTX:::\n";
        std::clog << thecam;

        // finally... the drawing!

        update();
    }

    void Viewport::resizeGL(int w, int h) {
        thecam.setAspect(w, h);
    }

    void Viewport::mousePressEvent(QMouseEvent * ev) {
        mouse_anchor = ev->localPos();
    }

    void Viewport::mouseMoveEvent(QMouseEvent * ev) {
        QPointF delta = ev->localPos() - mouse_anchor;

        if (movemod == Qt::NoModifier) { // orbit
            delta *= std::acos(-1) / 180.0;

            thecam.orbitViewX(delta.y());
            thecam.orbitWorldY(delta.x());
        } else if (movemod == Qt::ControlModifier) { // pan
            thecam.pan(-delta.x(), delta.y());
        } else if (movemod == Qt::ShiftModifier) { // zoom
            delta *= std::acos(-1) / 180.0;
            thecam.zoomChange(delta.y());
        } else if (movemod == (Qt::ControlModifier | Qt::ShiftModifier)) { // dolly
            thecam.dolly(delta.y());
        } else {
            ev->ignore();
            return;
        }

        mouse_anchor = ev->localPos();
        ev->accept();

        update();
    }

    void Viewport::wheelEvent(QWheelEvent * ev) {
        double zfac = ev->angleDelta().y() / 8.0 * std::acos(-1) / 180.0;

        thecam.zoomChange(-zfac);
        ev->accept();

        update();
    }

    void Viewport::keyPressEvent(QKeyEvent * ev) {
        // get the modifier keys, that's all we care about here
        movemod = static_cast<unsigned int>(ev->modifiers());

        // call base class anyway, since we probably ignored some other keys
        QOpenGLWidget::keyPressEvent(ev);
    }

    void Viewport::keyReleaseEvent(QKeyEvent * ev) {
        // just clear our movemod and then call base class method
        movemod = 0;
        QOpenGLWidget::keyReleaseEvent(ev);
    }

    void Viewport::initializeGL() {
        initializeOpenGLFunctions();

        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND);

        glGenVertexArrays(1, &render_vao);
        glBindVertexArray(render_vao);

        char * vshdsrc = R"(#version 450
in dvec3 posit;
out vec4 ocol;

uniform dmat4 ovmtx;
uniform dmat4 opmtx;

void main() {
    ocol = vec4(1.0, 1.0, 1.0, 0.25);
    gl_Position = vec4(vec3(posit), 1.0) * mat4(ovmtx) * mat4(opmtx);
})";

        char * fshdsrc = R"(#version 450
in vec4 ocol;
out vec4 oocol;

void main() {
    oocol = ocol;
})";

        GLuint vshd = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vshd, 1, &vshdsrc, nullptr);
        glCompileShader(vshd);

        checkshd(vshd);

        GLuint fshd = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fshd, 1, &fshdsrc, nullptr);
        glCompileShader(fshd);

        checkshd(fshd);

        render_shdprgm = glCreateProgram();
        glAttachShader(render_shdprgm, vshd);
        glAttachShader(render_shdprgm, fshd);

        glBindFragDataLocation(render_shdprgm, 0, "oocol");

        glLinkProgram(render_shdprgm);
    }

    void Viewport::paintGL() {
        if (!cando) {
            return;
        }

        // first, clear the screen
        glClearColor(0.0, 0.0, 0.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT);

        glBindVertexArray(render_vao);
        glUseProgram(render_shdprgm);

        // now to run the DL
        for (auto & i : renderThis) {
            if (command_cast<Command::VTX *>(i)) {
                Command::VTX * c_vtx = command_cast<Command::VTX *>(i);

                ROM::File sf = segmentFile(c_vtx->ram_address >> 24);

                auto fptr = sf.begin() + (c_vtx->ram_address & 0x00FFFFFF);

                for (size_t j = 0; j < c_vtx->size; j++) {
                    RawVertex rv;
                    rv.position[0] = be_s16(fptr); fptr += 2;
                    rv.position[1] = be_s16(fptr); fptr += 2;
                    rv.position[2] = be_s16(fptr); fptr += 2;
                    fptr += 2;
                    rv.tex_pos[0] = be_s16(fptr); fptr += 2;
                    rv.tex_pos[1] = be_s16(fptr); fptr += 2;

                    // for now, always assume lighting is off
                    rv.dummy_normflag = 0;
                    rv.RGB[0] = *fptr++;
                    rv.RGB[1] = *fptr++;
                    rv.RGB[2] = *fptr++;

                    rv.alpha = *fptr++;

                    vtx_preGL[c_vtx->dst_idx + j] = Vertex(rv);
                }
            } else if (command_cast<Command::TRI1 *>(i)) {
                Command::TRI1 * c_tri1 = command_cast<Command::TRI1 *>(i);

                drawNow(c_tri1->vtx_idxs.data(), 3);

            } else if (command_cast<Command::TRI2 *>(i)) {
                Command::TRI2 * c_tri2 = command_cast<Command::TRI2 *>(i);

                drawNow(c_tri2->vtx_idxs.data(), 6);
            } else if (command_cast<Command::ENDDL *>(i)) {
                break;
            } else if (command_cast<Command::SETCOMBINE *>(i)) {
                Command::SETCOMBINE * c_setc = command_cast<Command::SETCOMBINE *>(i);

                state_cc_1.ca = c_setc->color_1a;
                state_cc_1.cb = c_setc->color_1b;
                state_cc_1.cc = c_setc->color_1c;
                state_cc_1.cd = c_setc->color_1d;

                state_cc_1.aa = c_setc->alpha_1a;
                state_cc_1.ab = c_setc->alpha_1b;
                state_cc_1.ac = c_setc->alpha_1c;
                state_cc_1.ad = c_setc->alpha_1d;


                state_cc_2.ca = c_setc->color_2a;
                state_cc_2.cb = c_setc->color_2b;
                state_cc_2.cc = c_setc->color_2c;
                state_cc_2.cd = c_setc->color_2d;

                state_cc_2.aa = c_setc->alpha_2a;
                state_cc_2.ab = c_setc->alpha_2b;
                state_cc_2.ac = c_setc->alpha_2c;
                state_cc_2.ad = c_setc->alpha_2d;
            } else if (command_cast<Command::RDPSETOTHERMODE *>(i)) {
                Command::RDPSETOTHERMODE * c_som = command_cast<Command::RDPSETOTHERMODE *>(i);

                state_om.setAllH(c_som->high_bits);
                state_om.setAllL(c_som->low_bits);
            } else if (command_cast<Command::SETOTHERMODE_H *>(i)) {
                Command::SETOTHERMODE_H * c_somh = command_cast<Command::SETOTHERMODE_H *>(i);

                state_om.setValueH(c_somh->shift, c_somh->size, c_somh->value);
            } else if (command_cast<Command::SETOTHERMODE_L *>(i)) {
                Command::SETOTHERMODE_L * c_soml = command_cast<Command::SETOTHERMODE_L *>(i);

                state_om.setValueL(c_soml->shift, c_soml->size, c_soml->value);
            } else if (command_cast<Command::GEOMETRYMODE *>(i)) {
                Command::GEOMETRYMODE * c_geom = command_cast<Command::GEOMETRYMODE *>(i);

                state_gm.clear_many(c_geom->clear_this);
                state_gm.set_many(c_geom->set_this);
            }
        }

        std::clog << "CC1: " << state_cc_1 << "\n";
        std::clog << "CC2: " << state_cc_2 << "\n";
        std::clog << "OM: " << state_om << "\n";
        std::clog << "GM: " << state_gm << "\n";
    }

    void Viewport::drawNow(uint8_t * ebodata, size_t edsize) {
        // first, take all the pre-GL stuff and make it post-GL
        std::transform(vtx_preGL.begin(), vtx_preGL.end(), vtx_postGL.begin(),
                       [](const Vertex & a) { return GLVertex(a); });

        GLuint vbo;
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(GLVertex) * vtx_postGL.size(), vtx_postGL.data(), GL_STATIC_DRAW);

        GLuint ebo;
        glGenBuffers(1, &ebo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(size_t) * edsize, ebodata, GL_STATIC_DRAW);

        GLint posattr = glGetAttribLocation(render_shdprgm, "posit");

        glEnableVertexAttribArray(posattr);
        glVertexAttribLPointer(posattr, 3, GL_DOUBLE,
                               sizeof(GLVertex), (void*)offsetof(GLVertex, position));

        GLint ovmattr = glGetUniformLocation(render_shdprgm, "ovmtx");
        glUniformMatrix4dv(ovmattr, 1, GL_TRUE, thecam.viewMatrix().rawRowMajor().data());

        GLint opmattr = glGetUniformLocation(render_shdprgm, "opmtx");
        glUniformMatrix4dv(opmattr, 1, GL_TRUE, thecam.projMatrix().rawRowMajor().data());

        glDrawElements(GL_TRIANGLES, edsize, GL_UNSIGNED_BYTE, nullptr);
    }
}
/** \file
 *
 *  \brief Implementation of RCP camera.
 *
 */

#include "RCP/Camera.hpp"

#include <ostream>
#include <iostream>

namespace RCP {
    void Camera::recalc_linfac() {
        linfac = (pos - focus).length() / lasth;
    }

    Camera::Camera() : pos(0,0,0),
                       focus(0,0,0),
                       up(0,1,0),
                       yfov(45.0 * std::acos(-1) / 180.0),
                       aspect(4.0 / 3.0),
                       nearZ(0.1),
                       farZ(100.0) { }

    void Camera::orbitWorldY(double rads) {
        Vector shiftpos = pos - focus;

        // XXX this is a cheat; we're really orbiting around _screen_ Y
        // here. This is just until we know how to get blender's "turntable"
        // style horizontal rotation working.
        shiftpos *= Matrix::identity().rotation(-rads, up.normalized());

        pos = shiftpos + focus;
    }

    void Camera::orbitViewX(double rads) {
        // to use view's X, we need to figure out what vector would correspond
        // with view X in world coords
        Vector v_right = up.crossed(pos - focus).normalized();

        Vector shiftpos = pos - focus;

        shiftpos *= Matrix::identity().rotation(-rads, v_right);

        pos = shiftpos + focus;

        up *= Matrix::identity().rotation(-rads, v_right);
    }

    void Camera::pan(double xdelta, double ydelta) {
        // move the camera _and_ the focus point for the panning to work

        recalc_linfac();

        Vector v_right = up.crossed(pos - focus).normalized();
        Vector v_up = up.normalized();

        pos += v_right * xdelta * linfac;
        pos += v_up * ydelta * linfac;

        focus += v_right * xdelta * linfac;
        focus += v_up * ydelta * linfac;
    }

    void Camera::rotateViewZ(double rads) {
        Vector v_away = (pos - focus).normalized();

        Vector shiftup = up - pos;

        shiftup *= Matrix::identity().rotation(rads, v_away);

        up = shiftup;
    }

    void Camera::dolly(double dist) {
        recalc_linfac();

        Vector v_away = (pos - focus).normalized();

        pos += v_away * dist * linfac;
    }

    Matrix Camera::viewMatrix() const {
        return Matrix::lookAt(pos, focus, up);
    }

    void Camera::zoomChange(double delta) {
        yfov += delta;
        yfov = std::max(0.0, std::min(yfov, std::acos(-1)));
    }

    void Camera::setAspect(double w, double h) {
        lastw = w;
        lasth = h;
        aspect = w/h;
    }

    void Camera::setDepthRange(double n, double f) {
        nearZ = n;
        farZ = f;
    }

    Matrix Camera::projMatrix() const {
        return Matrix::perspective(yfov, aspect, nearZ, farZ);
    }

    std::ostream & operator<<(std::ostream & os, const Camera & cm) {
        os << "CAMERA @ " << cm.pos << " SEEING " << cm.focus << " UP-IS " << cm.up << "\n";
        os << "    FOV (rad): " << cm.yfov << " A/R: " << cm.aspect << "\n";
        os << "    Depth extents: " << cm.nearZ << ".." << cm.farZ << "\n";

        os << "  VMTX::" << cm.viewMatrix() << "  PMTX::" << cm.projMatrix();

        return os;
    }
}
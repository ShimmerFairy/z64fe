/** \file
 *
 *  \brief Implements the vertex stuff for the RCP
 *
 */

#include "RCP/Vertex.hpp"

namespace RCP {
    Vertex::Vertex(const RawVertex & rv) : position(rv.position[0], rv.position[1], rv.position[2]),
                                           tex_pos{rv.tex_pos[0], rv.tex_pos[1]} {

        if (rv.dummy_normflag) {
            normal = Vector(rv.normal[0], rv.normal[1], rv.normal[2]);
            RGBA[0] = RGBA[1] = RGBA[2] = RGBA[3] = 0;
        } else {
            normal = Vector(0,0,0);
            RGBA[0] = rv.RGB[0];
            RGBA[1] = rv.RGB[1];
            RGBA[2] = rv.RGB[2];
        }

        RGBA[3] = rv.alpha;
    }

    bool operator==(const Vertex & a, const Vertex & b) {
        return (a.position == b.position)
            && (a.normal == b.normal)
            && (a.tex_pos == b.tex_pos)
            && (a.RGBA == b.RGBA);
    }

    GLVertex::GLVertex(const Vertex & v) : position{v.position.x(), v.position.y(), v.position.z()},
                                           normal{v.normal.x(), v.normal.y(), v.normal.z()},
                                           tex_pos{v.tex_pos[0], v.tex_pos[1]},
                                           RGBA{v.RGBA[0], v.RGBA[1], v.RGBA[2], v.RGBA[3]} { }
}
/** \file
 *
 *  \brief Implement class functions for RCP state.
 *
 */

#include "RCP/State.hpp"
#include "RCP/exceptions.hpp"

#include <ostream>

namespace RCP {
    GeometryMode::GeometryMode() {
        flagmap[Flags::ZBUFFER] = false;
        flagmap[Flags::SHADE] = false;
        flagmap[Flags::CULL_FRONT] = false;
        flagmap[Flags::CULL_BACK] = false;
        flagmap[Flags::FOG] = false;
        flagmap[Flags::LIGHTING] = false;
        flagmap[Flags::TEXTURE_GEN] = false;
        flagmap[Flags::TEXTURE_GEN_LINEAR] = false;
        flagmap[Flags::SHADING_SMOOTH] = false;
    }

    uint32_t GeometryMode::asMask(Flags which) {
        switch (which) {
          case Flags::ZBUFFER:
            return 0x00000001;
            break;

          case Flags::SHADE:
            return 0x00000004;
            break;

          case Flags::CULL_FRONT:
            return 0x00000200;
            break;

          case Flags::CULL_BACK:
            return 0x00000400;
            break;

          case Flags::FOG:
            return 0x00010000;
            break;

          case Flags::LIGHTING:
            return 0x00020000;
            break;
          case Flags::TEXTURE_GEN:
            return 0x00040000;
            break;

          case Flags::TEXTURE_GEN_LINEAR:
            return 0x00080000;
            break;

          case Flags::SHADING_SMOOTH:
            return 0x00200000;
            break;
        }
    }

    void GeometryMode::clear_many(uint32_t bits) {
        uint32_t keep = ~bits;
        for (auto & i : flagmap) {
            i.second = (keep & asMask(i.first)) && i.second;
        }
    }

    void GeometryMode::set_many(uint32_t bits) {
        for (auto & i : flagmap) {
            i.second = (bits & asMask(i.first)) || i.second;
        }
    }

    void GeometryMode::set(Flags thatone) {
        flagmap.at(thatone) = true;
    }

    void GeometryMode::clear(Flags thatone) {
        flagmap.at(thatone) = false;
    }

    bool GeometryMode::check(Flags thatone) {
        return flagmap.at(thatone);
    }

    std::ostream & operator<<(std::ostream & os, const GeometryMode & gm) {
        os << "GeoMode[ ";
        for (auto & i : gm.flagmap) {
            os << static_cast<int>(i.first) << ":" << std::boolalpha << i.second;
            os << " ";
        }
        os << "]";

        return os;
    }

    auto OtherMode::fromShiftH(uint8_t sft) -> Fields {
        switch (sft) {
          case 4:
            return Fields::ALPHADITHER;
            break;

          case 6:
            return Fields::RGBDITHER;
            break;

          case 8:
            return Fields::COMBKEY;
            break;

          case 9:
            return Fields::TEXCONV;
            break;

          case 12:
            return Fields::TEXFILT;
            break;

          case 14:
            return Fields::TEXLUT;
            break;

          case 16:
            return Fields::TEXLOD;
            break;

          case 17:
            return Fields::TEXDETAIL;
            break;

          case 19:
            return Fields::TEXPERSP;
            break;

          case 20:
            return Fields::CYCLETYPE;
            break;

          case 23:
            return Fields::PIPELINE;
            break;

          default:
            throw X::RCP::BadOtherMode("high", sft, -1, -1);
            break;
        }
    }

    auto OtherMode::fromShiftL(uint8_t sft) -> Fields {
        switch (sft) {
          case 0:
            return Fields::ALPHACOMPARE;
            break;

          case 2:
            return Fields::ZSRCREL;
            break;

          case 3:
            return Fields::RENDERMODE;
            break;

          default:
            throw X::RCP::BadOtherMode("low", sft, -1, -1);
            break;
        }
    }

    void OtherMode::setValueH(uint8_t sft, uint8_t siz, uint32_t rawval) {
        uint32_t real = (rawval >> sft) & ((1 << siz) - 1);

        switch (fromShiftH(sft)) {
          case Fields::ALPHADITHER:
            switch (real) {
              case 0b00:
                ad = AlphaDither::PATTERN;
                break;

              case 0b01:
                ad = AlphaDither::NOTPATTERN;
                break;

              case 0b10:
                ad = AlphaDither::NOISE;
                break;

              case 0b11:
                ad = AlphaDither::DISABLE;
                break;

              default:
                throw X::RCP::BadOtherMode("high", sft, siz, rawval);
                break;
            }
            break;

          case Fields::RGBDITHER:
            switch (real) {
              case 0b00:
                cd = RGBDither::MAGICSQ;
                break;

              case 0b01:
                cd = RGBDither::BAYER;
                break;

              case 0b10:
                cd = RGBDither::NOISE;
                break;

              case 0b11:
                cd = RGBDither::DISABLE;
                break;

              default:
                throw X::RCP::BadOtherMode("high", sft, siz, rawval);
                break;
            }
            break;

          case Fields::COMBKEY:
            ck = real;
            break;

          case Fields::TEXCONV:
            switch (real) {
              case 0b000:
                tc = TexConv::CONV;
                break;

              case 0b101:
                tc = TexConv::FILTCONV;
                break;

              case 0b110:
                tc = TexConv::FILT;
                break;

              default:
                throw X::RCP::BadOtherMode("high", sft, siz, rawval);
                break;
            }
            break;

          case Fields::TEXFILT:
            switch (real) {
              case 0b00:
                tf = TexFilt::POINT;
                break;

              case 0b10:
                tf = TexFilt::BILERP;
                break;

              case 0b11:
                tf = TexFilt::AVERAGE;
                break;

              default:
                throw X::RCP::BadOtherMode("high", sft, siz, rawval);
                break;
            }
            break;

          case Fields::TEXLUT:
            switch (real) {
              case 0b00:
                tt = TexLUT::NONE;
                break;

              case 0b10:
                tt = TexLUT::RGBA16;
                break;

              case 0b11:
                tt = TexLUT::IA16;
                break;

              default:
                throw X::RCP::BadOtherMode("high", sft, siz, rawval);
                break;
            }
            break;

          case Fields::TEXLOD:
            tl = real;
            break;

          case Fields::TEXDETAIL:
            switch (real) {
              case 0b00:
                td = TexDetail::CLAMP;
                break;

              case 0b01:
                td = TexDetail::SHARPEN;
                break;

              case 0b10:
                td = TexDetail::DETAIL;
                break;

              default:
                throw X::RCP::BadOtherMode("high", sft, siz, rawval);
                break;
            }
            break;

          case Fields::TEXPERSP:
            tp = real;
            break;

          case Fields::CYCLETYPE:
            switch (real) {
              case 0b00:
                cyc = CycleType::ONE;
                break;

              case 0b01:
                cyc = CycleType::TWO;
                break;

              case 0b10:
                cyc = CycleType::COPY;
                break;

              case 0b11:
                cyc = CycleType::FILL;
                break;

              default:
                throw X::RCP::BadOtherMode("high", sft, siz, rawval);
                break;
            }
            break;

          case Fields::PIPELINE:
            pm = real;
            break;

          default:
            throw X::RCP::BadOtherMode("high", sft, siz, rawval);
            break;
        }
    }

    void OtherMode::setValueL(uint8_t sft, uint8_t siz, uint32_t rawval) {
        uint32_t real = (rawval >> sft) & ((1 << siz) - 1);

        switch (fromShiftL(sft)) {
          case Fields::ALPHACOMPARE:
            switch (real) {
              case 0b00:
                ac = AlphaCompare::NONE;
                break;

              case 0b01:
                ac = AlphaCompare::THRESHOLD;
                break;

              case 0b11:
                ac = AlphaCompare::DITHER;
                break;

              default:
                throw X::RCP::BadOtherMode("low", sft, siz, rawval);
                break;
            }
            break;

          case Fields::ZSRCREL:
            zs = real;
            break;

          case Fields::RENDERMODE:
            bl.setMode(real);
            break;

          default:
            throw X::RCP::BadOtherMode("low", sft, siz, rawval);
            break;
        }
    }

    void OtherMode::setAllH(uint32_t rawval) {
        setValueH(4, 2, rawval);
        setValueH(6, 2, rawval);
        setValueH(8, 1, rawval);
        setValueH(9, 3, rawval);
        setValueH(12, 2, rawval);
        setValueH(14, 2, rawval);
        setValueH(16, 1, rawval);
        setValueH(17, 2, rawval);
        setValueH(19, 1, rawval);
        setValueH(20, 2, rawval);
        setValueH(23, 1, rawval);
    }

    void OtherMode::setAllL(uint32_t rawval) {
        setValueL(0, 2, rawval);
        setValueL(2, 1, rawval);
        setValueL(3, 29, rawval);
    }

    std::ostream & operator<<(std::ostream & os, const OtherMode & om) {
        os << "OTHERMODE[" << std::boolalpha;

        os << " ALPHACOMP:" << static_cast<int>(om.ac)
           << " ZSRCSEL:" << om.zs
           << " " << om.bl
           << " ALPHADITHER:" << static_cast<int>(om.ad)
           << " RGBDITHER:" << static_cast<int>(om.cd)
           << " COMBKEY:" << om.ck
           << " TEXCONV:" << static_cast<int>(om.tc)
           << " TEXFILT:" << static_cast<int>(om.tf)
           << " TEXLUT:" << static_cast<int>(om.tt)
           << " TEXLOD:" << om.tl
           << " TEXDETAIL:" << static_cast<int>(om.td)
           << " TEXPERSP:" << om.tp
           << " CYCLETYPE:" << static_cast<int>(om.cyc)
           << " PIPELINE:" << om.pm;

        os << " ]";

        return os;
    }

    auto BLMode::getP(uint8_t val) -> ParamP {
        switch (val) {
          case 0b00:
            return ParamP::IN;
            break;

          case 0b01:
            return ParamP::MEM;
            break;

          case 0b10:
            return ParamP::BL;
            break;

          case 0b11:
            return ParamP::FOG;
            break;
        }
    }

    auto BLMode::getM(uint8_t val) -> ParamM {
        return getP(val);
    }

    auto BLMode::getA(uint8_t val) -> ParamA {
        switch (val) {
          case 0b00:
            return ParamA::IN;
            break;

          case 0b01:
            return ParamA::FOG;
            break;

          case 0b10:
            return ParamA::SHADE;
            break;

          case 0b11:
            return ParamA::ZERO;
            break;
        }
    }

    auto BLMode::getB(uint8_t val) -> ParamB {
        switch (val) {
          case 0b00:
            return ParamB::ONEMINUSA;
            break;

          case 0b01:
            return ParamB::AMEM;
            break;

          case 0b10:
            return ParamB::ONE;
            break;

          case 0b11:
            return ParamB::ZERO;
            break;
        }
    }

    void BLMode::setMode(uint32_t val) {
        // extra 0' at the start for highlighters that don't handle digit
        // separator yet. Blame C++ for making one of the dumber choices for
        // digit separator.
        aa_en      = val & 0b0'0000'0000'0000'1;
        z_cmp      = val & 0b0'0000'0000'0001'0;
        z_upd      = val & 0b0'0000'0000'0010'0;
        im_rd      = val & 0b0'0000'0000'0100'0;
        clr_on_cvg = val & 0b0'0000'0000'1000'0;

        switch ((val >> 5) & 0b11) {
          case 0b00:
            cvg = CvgDst::CLAMP;
            break;

          case 0b01:
            cvg = CvgDst::WRAP;
            break;

          case 0b10:
            cvg = CvgDst::FULL;
            break;

          case 0b11:
            cvg = CvgDst::SAVE;
            break;
        }

        switch ((val >> 7) & 0b11) {
          case 0b00:
            zm = ZMode::OPA;
            break;

          case 0b01:
            zm = ZMode::INTER;
            break;

          case 0b10:
            zm = ZMode::XLU;
            break;

          case 0b11:
            zm = ZMode::DEC;
            break;
        }

        cvgXalpha   = val & 0b0'0001'0000'0000'0;
        alphacvgsel = val & 0b0'0010'0000'0000'0;
        forceBL     = val & 0b0'0100'0000'0000'0;

        uint16_t cycbits = val >> 13;

        c1p = getP(cycbits >> 14 & 0b11);
        c2p = getP(cycbits >> 12 & 0b11);
        c1a = getA(cycbits >> 10 & 0b11);
        c2a = getA(cycbits >>  8 & 0b11);
        c1m = getM(cycbits >>  6 & 0b11);
        c2m = getM(cycbits >>  4 & 0b11);
        c1b = getB(cycbits >>  2 & 0b11);
        c2b = getB(cycbits       & 0b11);
    }

    std::ostream & operator<<(std::ostream & os, const BLMode & blm) {
        os << "BLMode[";
        os << std::boolalpha;
        os << " AA_EN:" << blm.aa_en
           << " Z_CMP:" << blm.z_cmp
           << " Z_UPD:" << blm.z_upd
           << " IM_RD:" << blm.im_rd
           << " CLR_ON_CVG: " << blm.clr_on_cvg
           << " CVG: " << static_cast<int>(blm.cvg)
           << " ZMODE: " << static_cast<int>(blm.zm)
           << " CVG_X_ALPHA: " << blm.cvgXalpha
           << " ALPHACVGSEL: " << blm.alphacvgsel
           << " FORCEBL: " << blm.forceBL;

        os << " CYCLE1_PMAB["
           << " " << static_cast<int>(blm.c1p)
           << " " << static_cast<int>(blm.c1m)
           << " " << static_cast<int>(blm.c1a)
           << " " << static_cast<int>(blm.c1b)
           << " ]";

        os << " CYCLE2_PMAB["
           << " " << static_cast<int>(blm.c2p)
           << " " << static_cast<int>(blm.c2m)
           << " " << static_cast<int>(blm.c2a)
           << " " << static_cast<int>(blm.c2b)
           << " ]";

        os << " ]";

        return os;
    }

    std::ostream & operator<<(std::ostream & os, const CCCycle & ccc) {
        os << "CCC[";
        os << " COLOR_A:" << static_cast<int>(ccc.ca);
        os << " COLOR_B:" << static_cast<int>(ccc.cb);
        os << " COLOR_C:" << static_cast<int>(ccc.cc);
        os << " COLOR_D:" << static_cast<int>(ccc.cd);

        os << " ALPHA_A:" << static_cast<int>(ccc.aa);
        os << " ALPHA_B:" << static_cast<int>(ccc.ab);
        os << " ALPHA_C:" << static_cast<int>(ccc.ac);
        os << " ALPHA_D:" << static_cast<int>(ccc.ad);
        os << " ]";

        return os;
    }
}